(require 'package)
	(package-initialize)
	(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
	(add-to-list 'package-archives '("elpy" . "https://jorgenschaefer.github.io/packages/"))
;;	(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(setq custom-enabled-themes '(DrOps))
(put 'denote-mode 'safe-local-variable (lambda (_) t))

(setq package-selected-packages
      '(
	0xc                            ; Base conversion made easy
	ag                             ; A front-end for ag ('the silver searcher'), the C ack replacement.
	all-the-icons                  ; A library for inserting Developer icons
	all-the-icons-dired            ; Shows icons for each file in dired mode
	calibredb                      ; Yet another calibre client
	centaur-tabs                   ; Aesthetic, modern looking customizable tabs plugin
	command-log-mode               ; log keyboard commands to buffer
	company-ledger                 ; Fuzzy auto-completion for Ledger & friends
	consult                        ; Consulting completing-read
	consult-company                ; Consult frontend for company
	consult-dir                    ; Insert paths into the minibuffer prompt
	consult-projectile             ; Consult integration for projectile
	consult-recoll                 ; Recoll queries using consult
	consult-yasnippet              ; A consulting-read interface for yasnippet
	corral                         ; Quickly surround text with delimiters
	crontab-mode                   ; Major mode for crontab(5)
	denote                         ; Simple notes with an efficient file-naming scheme
	dictcc                         ; Look up translations on dict.cc
	dklrt                          ; Ledger Recurring Transactions.
	elfeed-goodies                 ; Elfeed goodies
	elfeed-org                     ; Configure elfeed with one or more org-mode files
	elpher                         ; A friendly gopher and gemini client
	elpy                           ; Emacs Python Development Environment
	embark                         ; Conveniently act on minibuffer completions
	embark-consult                 ; Consult integration for Embark
	epresent                       ; Simple presentation mode for Emacs Org-mode
	fill-column-indicator          ; Graphically indicate the fill column
	flymake                        ; A universal on-the-fly syntax checker
	german-holidays                ; German holidays for Emacs calendar
	gnuplot                        ; Major-mode and interactive frontend for gnuplot
	go-translate                   ; Translation framework supports multiple engines such as Google/Bing/DeepL
	htmlize                        ; Convert buffer text and decorations to HTML.
	ibuffer-projectile             ; Group ibuffer's list by projectile root
	imenu-anywhere                 ; ido/ivy/helm imenu across same mode/project/etc buffers
	json-navigator                 ; View and navigate JSON structures
	lorem-ipsum                    ; Insert dummy pseudo Latin text.
	magit-gitflow                  ; gitflow extension for magit
	major-mode-hydra               ; Major mode keybindings managed by Hydra
	major-mode-icons               ; display icon for major-mode on mode-line.
	marginalia                     ; Enrich existing commands with completion annotations
	markdown-changelog             ; Maintain changelog entries
	markdown-mode+                 ; extra functions for markdown-mode
	mastodon                       ; Client for Mastodon, a federated social network
	neotree                        ; A tree plugin like NerdTree for Vim
	nikola                         ; Simple wrapper for nikola
	nov                            ; Featureful EPUB reader mode
	openwith                       ; Open files with external programs
	orderless                      ; Completion style for matching regexps in any order
	org-caldav                     ; Sync org files with external calendar through CalDAV
	org-chef                       ; Cookbook and recipe management with org-mode.
	org-download                   ; Image drag-and-drop for Org-mode.
	org-edna                       ; Extensible Dependencies 'N' Actions
	org-link-beautify              ; Beautify Org Links
	org-mime                       ; org html export for text/html MIME emails
	org-pdfview                    ; Support for links to documents in pdfview mode
	org-ref                        ; citations, cross-references and bibliographies in org-mode
	org-roam                       ; A database abstraction layer for Org-mode
	org-tag-beautify               ; Beautify Org mode tags
	osm                            ; OpenStreetMap viewer
	outline-magic                  ; outline mode extensions for Emacs
	pass                           ; Major mode for password-store.el
	persp-projectile               ; Perspective integration with Projectile
	perspective                    ; switch between named "perspectives" of the editor
	projectile                     ; Manage and navigate projects in Emacs easily
	projectile-ripgrep             ; Run ripgrep with Projectile
	rainbow-delimiters             ; Highlight brackets according to their depth
	recentf-ext                    ; Recentf extensions
	runner                         ; Improved "open with" suggestions for dired
	ssh                            ; Support for remote logins using ssh.
	status                         ; This package adds support for status icons to Emacs.
	sudo-edit                      ; Open files as another user
	treemacs                       ; A tree style file explorer package
	treemacs-all-the-icons         ; all-the-icons integration for treemacs
	treemacs-icons-dired           ; Treemacs icons for dired
	treemacs-projectile            ; Projectile integration for treemacs
	unify-opening                  ; Unify the mechanism to open files
	vertico                        ; VERTical Interactive COmpletion
	vertico-posframe               ; Using posframe to show Vertico
	win-switch                     ; fast, dynamic bindings for window-switching/resizing
	writeroom-mode                 ; Minor mode for distraction-free writing
	yafolding                      ; Folding code blocks based on indentation
	yankpad
	))
(package-install-selected-packages)

(cua-mode t)
(setq cua-auto-tabify-rectangles nil) ;; Don't tabify after rectangle commands
(transient-mark-mode 1) ;; No region when it is not highlighted
(setq cua-keep-region-after-copy nil) ;; Standard Windows behaviour

(defun my-window-select (window &rest _)
      "Select WINDOW.

      Use this as the `body-function' in a `display-buffer-alist' entry."
      (select-window window)
    )

      ;; NOTE 2023-03-17: Remember that I am using development versions of
      ;; Emacs.  Some of my `display-buffer-alist' contents are for Emacs
      ;; 29+.
	(setq display-buffer-alist
	      `(;; no window
		("\\`\\*Async Shell Command\\*\\'"
		 (display-buffer-no-window))
		("\\`\\*\\(Warnings\\|Compile-Log\\|Org Links\\)\\*\\'"
		 (display-buffer-no-window)
		 (allow-no-window . t))
		;; bottom side window
		("\\*Org \\(Select\\|Note\\)\\*" ; the `org-capture' key selection and `org-add-log-note'
		 (display-buffer-in-side-window)
		 (dedicated . t)
		 (side . bottom)
		 (slot . 0)
		 (window-parameters . ((mode-line-format . none))))


		;; bottom buffer (NOT side window)
		((or . ((derived-mode . flymake-diagnostics-buffer-mode)
			(derived-mode . flymake-project-diagnostics-mode)
			(derived-mode . messages-buffer-mode)
			(derived-mode . backtrace-mode)))

		 (display-buffer-reuse-mode-window
		  display-buffer-at-bottom)

		 (window-height . 0.3)
		 (dedicated . t)
		 (preserve-size . (t . t)))

		("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
	       nil
	       (window-parameters (mode-line-format . none)))


		("\\*Embark Actions\\*"
		 (display-buffer-reuse-mode-window display-buffer-in-side-window display-buffer-below-selected)
		 (window-height . fit-window-to-buffer)
		 (window-parameters . ((no-other-window . t)
				       (mode-line-format . none))))

		("\\*\\(Output\\|Register Preview\\).*"
		 (display-buffer-reuse-mode-window display-buffer-at-bottom))

		;; below current window
		("\\(\\*Capture\\*\\|CAPTURE-.*\\)"
		 (display-buffer-reuse-mode-window display-buffer-below-selected))

		("\\*\\vc-\\(incoming\\|outgoing\\|git : \\).*"
		 (display-buffer-reuse-mode-window display-buffer-below-selected)
		 (window-height . 0.1)
		 (dedicated . t)
		 (preserve-size . (t . t)))

((derived-mode . reb-mode) ; M-x re-builder
		 (display-buffer-reuse-mode-window display-buffer-below-selected)
		 (window-height . 4) ; note this is literal lines, not relative
		 (dedicated . t)
		 (preserve-size . (t . t)))
		((or . ((derived-mode . occur-mode)
			(derived-mode . grep-mode)
			(derived-mode . Buffer-menu-mode)
			(derived-mode . log-view-mode)
			(derived-mode . help-mode) ; See the hooks for `visual-line-mode'
			"\\*\\(|Buffer List\\|Occur\\|vc-change-log\\).*"
			))
		 (display-buffer-reuse-mode-window display-buffer-pop-up-window )
		 (dedicated . t)
		 (body-function . my-window-select))

		("\\*\\(Calendar\\|Bookmark Annotation\\|ert\\).*"
		 (display-buffer-reuse-mode-window display-buffer-below-selected)
		 (dedicated . t)
		 (window-height . fit-window-to-buffer))


		;; NOTE 2023-02-17: `man' does not fully obey the
		;; `display-buffer-alist'.  It works for new frames and for
		;; `display-buffer-below-selected', but otherwise is
		;; unpredictable.  See `Man-notify-method'.
		((or . ((derived-mode . Man-mode)
			(derived-mode . woman-mode)
			"\\*\\(Man\\|woman\\).*"))
		 (display-buffer-same-window))))

(require 'openwith)
(openwith-mode t)
;;(add-to-list  'mm-inhibit-file-name-handlers 'openwith-file-handler)
(setq openwith-associations
      (list (list (openwith-make-extension-regexp '("pdf"))
                  "evince" '(file))
            (list (openwith-make-extension-regexp '("maff" "mht" "mhtml"))
                  "firefox" '(file))
            (list (openwith-make-extension-regexp '("m4a" "flac" "mp3" "wav"))
                  "vlc" '(file))
            (list (openwith-make-extension-regexp '("avi" "flv" "mov" "mp4"
                                                    "mpeg" "mpg" "ogg" "wmv"))
                  "vlc" '(file))
            (list (openwith-make-extension-regexp '("doc" "docx" "odt"))
                  "libreoffice" '("--writer" file))
            (list (openwith-make-extension-regexp '("ods" "xls" "xlsx"))
                  "libreoffice" '("--calc" file))
            (list (openwith-make-extension-regexp '("odp" "pps" "ppt" "pptx"))
                  "libreoffice" '("--impress" file))
            ))

(require 'json)

(load (concat user-emacs-directory "reveal.el"))

(elpy-enable)
(add-to-list 'auto-mode-alist '("\\.org.txt\\'" . org-mode))
(add-to-list 'auto-mode-alist '("\\.ino\\'" . c-mode))
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")

;;; yasnippet
;;; should be loaded before auto complete so that they can work together
(require 'yasnippet)

;; Check if system is Microsoft Windows
(defun my-system-type-is-windows ()
  "Return true if system is Windows-based (at least up to Win7)"
  (string-equal system-type "windows-nt")
  )

;; Check if system is GNU/Linux
(defun my-system-type-is-linux ()
  "Return true if system is GNU/Linux-based"
  (string-equal system-type "gnu/linux")
  )

(defun split-window-really-sensibly (&optional window)
  (let ((window (or window (selected-window))))
    (or (and (window-splittable-p window t)
	     ;; Split window vertically.
	     (with-selected-window window
	       (split-window-right))) 
	(and (window-splittable-p window)
	     ;; Split window horizontally.
	     (with-selected-window window
	       (split-window-below)))
	(and (eq window (frame-root-window (window-frame window)))
	     (not (window-minibuffer-p window))
	     ;; If WINDOW is the only window on its frame and is not the
	     ;; minibuffer window, try to split it vertically disregarding
	     ;; the value of `split-height-threshold'.
	     (let ((split-height-threshold 0))
	       (when (window-splittable-p window)
		 (with-selected-window window
		   (split-window-right))))))))
(setq split-window-preferred-function 'split-window-really-sensibly)

(require 'denote)

  ;; Remember to check the doc strings of those variables.
  (setq denote-directory (expand-file-name "~/wiki/"))
  (setq denote-known-keywords '("emacs" "philosophy" "politics" "economics"))
  (setq denote-infer-keywords t)
  (setq denote-sort-keywords t)
  (setq denote-file-type 'org) ; Org is the default, set others here
  (setq denote-prompts '(title keywords))


  ;; Pick dates, where relevant, with Org's advanced interface:
  (setq denote-date-prompt-use-org-read-date t)


  ;; Read this manual for how to specify `denote-templates'.  We do not
  ;; include an example here to avoid potential confusion.


  ;; We allow multi-word keywords by default.  The author's personal
  ;; preference is for single-word keywords for a more rigid workflow.
  (setq denote-allow-multi-word-keywords 0)

  (setq denote-date-format nil) ; read doc string

  ;; By default, we fontify backlinks in their bespoke buffer.
  (setq denote-link-fontify-backlinks t)

  ;; Also see `denote-link-backlinks-display-buffer-action' which is a bit
  ;; advanced.
  (setq denote-link-backlinks-display-buffer-action
	'((display-buffer-reuse-window
	   display-buffer-in-side-window)
	  (side . right)
	  (slot . 99)
	  (window-width . 0.3)))
  ;; If you use Markdown or plain text files (Org renders links as buttons
  ;; right away)
  ;; (add-hook 'find-file-hook #'denote-link-buttonize-buffer)

  ;; We use different ways to specify a path for demo purposes.
  (setq denote-dired-directories
	(list denote-directory
	      (thread-last denote-directory (expand-file-name "attachments"))
	      (expand-file-name "~/Documents/books")))

  ;; Generic (great if you rename files Denote-style in lots of places):
  ;; (add-hook 'dired-mode-hook #'denote-dired-mode)
  ;;
  ;; OR if only want it in `denote-dired-directories':
  (add-hook 'dired-mode-hook #'denote-dired-mode-in-directories)

  ;; Here is a custom, user-level command from one of the examples we
  ;; showed in this manual.  We define it here and add it to a key binding
  ;; below.
  (defun my-denote-journal ()
    "Create an entry tagged 'journal', while prompting for a title."
    (interactive)
    (denote
     (denote--title-prompt)
     '("journal")))

  ;; Denote DOES NOT define any key bindings.  This is for the user to
  ;; decide.  For example:
  (let ((map global-map))
    (define-key map (kbd "C-c n j") #'my-denote-journal) ; our custom command
    (define-key map (kbd "C-c n n") #'denote)
    (define-key map (kbd "C-c n N") #'denote-type)
    (define-key map (kbd "C-c n d") #'denote-date)
    (define-key map (kbd "C-c n s") #'denote-subdirectory)
    (define-key map (kbd "C-c n t") #'denote-template)
    ;; If you intend to use Denote with a variety of file types, it is
    ;; easier to bind the link-related commands to the `global-map', as
    ;; shown here.  Otherwise follow the same pattern for `org-mode-map',
    ;; `markdown-mode-map', and/or `text-mode-map'.
    (define-key map (kbd "C-c n i") #'denote-link) ; "insert" mnemonic
    (define-key map (kbd "C-c n I") #'denote-link-add-links)
    (define-key map (kbd "C-c n l") #'denote-link-find-file) ; "list" links
    (define-key map (kbd "C-c n b") #'denote-link-backlinks)
    ;; Note that `denote-rename-file' can work from any context, not just
    ;; Dired bufffers.  That is why we bind it here to the `global-map'.
    (define-key map (kbd "C-c n r") #'denote-rename-file)
    (define-key map (kbd "C-c n R") #'denote-rename-file-using-front-matter))

  ;; Key bindings specifically for Dired.
  (let ((map dired-mode-map))
    (define-key map (kbd "C-c C-d C-i") #'denote-link-dired-marked-notes)
    (define-key map (kbd "C-c C-d C-r") #'denote-dired-rename-marked-files)
    (define-key map (kbd "C-c C-d C-R") #'denote-dired-rename-marked-files-using-front-matter))

  (with-eval-after-load 'org-capture
    (setq denote-org-capture-specifiers "%l\n%i\n%?")
    (add-to-list 'org-capture-templates
		 '("n" "New note (with denote.el)" plain
		   (file denote-last-path)
		   #'denote-org-capture
		   :no-save t
		   :immediate-finish nil
		   :kill-buffer t
		   :jump-to-captured t)))

  ;; Also check the commands `denote-link-after-creating',
  ;; `denote-link-or-create'.  You may want to bind them to keys as well.
(defvar my-denote-chordpro-front-matter
  "#!/bin/sh

# title:      %s
# date:       %s
# tags:       %s
# identifier: %s
\n"
  "Demo shell script front matter.
It is passed to `format' with arguments TITLE, DATE, KEYWORDS,
ID.  Advanced users are advised to consult Info node `(denote)
Change the front matter format'.")

  (add-to-list 'denote-file-types
	 '(ChordPro
	   :extension ".cho"
	   :date-function denote-date-org-timestamp
	   :front-matter denote-org-front-matter
	   :title-key-regexp "^#\\+title\\s-*:"
	   :title-value-function identity
	   :title-value-reverse-function denote-trim-whitespace
	   :keywords-key-regexp "^#\\+filetags\\s-*:"
	   :keywords-value-function denote-format-keywords-for-org-front-matter
	   :keywords-value-reverse-function denote-extract-keywords-from-front-matter
	   :link denote-org-link-format
	   :link-in-context-regexp denote-org-link-in-context-regexp)
	  )

    (define-minor-mode denote-mode
      "Denote is a simple note-taking
      tool for Emacs. It is based on the idea that notes should follow
      a predictable and descriptive file-naming scheme. The file name
      must offer a clear indication of what the note is about, without
      reference to any other metadata. Denote basically streamlines
      the creation of such files while providing facilities to link
      between them. 

      Denote's file-naming scheme is not limited to notes. It can be used
      for all types of file, including those that are not editable in
      Emacs, such as videos. Naming files in a constistent way makes
      their filtering and retrieval considerably easier. Denote
      provides relevant facilities to rename files, regardless of file
      type."
      :lighter " Note"
      :keymap (let ((map
      (make-sparse-keymap)))
		(define-key map (kbd "C-l") 'denote-link-or-create)
		(define-key map (kbd "C-n") 'denote-link-after-creating)
		(define-key map (kbd "<f6>")(lambda () (interactive) (find-file "~/wiki")))
		map))

(require 'recentf)
(recentf-mode 1)
(setq recentf-max-saved-items 25)
(setq recentf-max-menu-items 40)
(setq recentf-menu-append-commands-flag t)
(setq recentf-menu-filter 'recentf-arrange-by-dir)

;;(add-hook 'org-mode-hook 'read-only-mode)
(setq initial-buffer-choice "~/org/ops.org")
(setq safe-local-variable-values
 '((org-image-actual-width . 50)
   (org-roam-mode . t)
   (org-roam-directory . "~/org/odo/")
   (initial-major-mode . dokuwiki-mode)))

(require 'org)
(require 'ox-html)
(require 'denote)

;;; Custom configuration for the export.

;;; Add any custom configuration that you would like to 'conf.el'.
(setq nikola-use-pygments t
      org-export-with-toc nil
      org-export-with-section-numbers nil
      org-startup-folded 'showeverything)

;; Load additional configuration from conf.el
(let ((conf (expand-file-name "conf.el" (file-name-directory load-file-name))))
  (if (file-exists-p conf)
      (load-file conf)))

;;; Macros

;; Load Nikola macros
(setq nikola-macro-templates
      (with-current-buffer
          (find-file
           (expand-file-name "macros.org" (file-name-directory load-file-name)))
        (org-macro--collect-macros)))

;;; Code highlighting
(defun org-html-decode-plain-text (text)
  "Convert HTML character to plain TEXT. i.e. do the inversion of
     `org-html-encode-plain-text`. Possible conversions are set in
     `org-html-protect-char-alist'."
  (mapc
   (lambda (pair)
     (setq text (replace-regexp-in-string (cdr pair) (car pair) text t t)))
   (reverse org-html-protect-char-alist))
  text)

;; Use pygments highlighting for code
(defun pygmentize (lang code)
  "Use Pygments to highlight the given code and return the output"
  (with-temp-buffer
    (insert code)
    (let ((lang (or (cdr (assoc lang org-pygments-language-alist)) "text")))
      (shell-command-on-region (point-min) (point-max)
                               (format "pygmentize -f html -l %s" lang)
                               (buffer-name) t))
    (buffer-string)))

(defconst org-pygments-language-alist
  '(("asymptote" . "asymptote")
    ("awk" . "awk")
    ("c" . "c")
    ("c++" . "cpp")
    ("cpp" . "cpp")
    ("clojure" . "clojure")
    ("css" . "css")
    ("d" . "d")
    ("emacs-lisp" . "scheme")
    ("F90" . "fortran")
    ("gnuplot" . "gnuplot")
    ("groovy" . "groovy")
    ("haskell" . "haskell")
    ("java" . "java")
    ("js" . "js")
    ("julia" . "julia")
    ("latex" . "latex")
    ("lisp" . "lisp")
    ("makefile" . "makefile")
    ("matlab" . "matlab")
    ("mscgen" . "mscgen")
    ("ocaml" . "ocaml")
    ("octave" . "octave")
    ("perl" . "perl")
    ("picolisp" . "scheme")
    ("python" . "python")
    ("r" . "r")
    ("ruby" . "ruby")
    ("sass" . "sass")
    ("scala" . "scala")
    ("scheme" . "scheme")
    ("sh" . "sh")
    ("sql" . "sql")
    ("sqlite" . "sqlite3")
    ("tcl" . "tcl"))
  "Alist between org-babel languages and Pygments lexers.
lang is downcased before assoc, so use lowercase to describe language available.
See: http://orgmode.org/worg/org-contrib/babel/languages.html and
http://pygments.org/docs/lexers/ for adding new languages to the mapping.")

;; Override the html export function to use pygments
(defun org-html-src-block (src-block contents info)
  "Transcode a SRC-BLOCK element from Org to HTML.
CONTENTS holds the contents of the item.  INFO is a plist holding
contextual information."
  (if (org-export-read-attribute :attr_html src-block :textarea)
      (org-html--textarea-block src-block)
    (let ((lang (org-element-property :language src-block))
          (code (org-element-property :value src-block))
          (code-html (org-html-format-code src-block info)))
      (if nikola-use-pygments
          (progn
            (unless lang (setq lang ""))
            (pygmentize (downcase lang) (org-html-decode-plain-text code)))
        code-html))))

;; Export images with custom link type
(defun org-custom-link-img-url-export (path desc format)
  (cond
   ((eq format 'html)
    (format "<img src=\"%s\" alt=\"%s\"/>" path desc))))
(org-add-link-type "img-url" nil 'org-custom-link-img-url-export)

;; Export images with built-in file scheme
(defun org-file-link-img-url-export (path desc format)
  (cond
   ((eq format 'html)
    (format "<img src=\"/%s\" alt=\"%s\"/>" path desc))))
(org-add-link-type "file" nil 'org-file-link-img-url-export)

;; Support for magic links (link:// scheme)
(org-link-set-parameters
  "link"
  :export (lambda (path desc backend)
             (cond
               ((eq 'html backend)
                (format "<a href=\"link:%s\">%s</a>"
                        path (or desc path))))))

;; Export function used by Nikola.
(defun nikola-html-export (infile outfile)
  "Export the body only of the input file and write it to
specified location."
  (with-current-buffer (find-file infile)
    (org-macro-replace-all nikola-macro-templates)
    (org-html-export-as-html nil nil t t)
    (write-file outfile nil)))

(defun org-html-headline (headline contents info)
  "Transcode a HEADLINE element from Org to HTML.
CONTENTS holds the contents of the headline.  INFO is a plist
holding contextual information."
  (unless (org-element-property :footnote-section-p headline)
    (let* ((numberedp (org-export-numbered-headline-p headline info))
           (numbers (org-export-get-headline-number headline info))
           (level (+ (org-export-get-relative-level headline info)
                     (1- (plist-get info :html-toplevel-hlevel))))
           (todo (and (plist-get info :with-todo-keywords)
                      (let ((todo (org-element-property :todo-keyword headline)))
                        (and todo (org-export-data todo info)))))
           (todo-type (and todo (org-element-property :todo-type headline)))
           (priority (and (plist-get info :with-priority)
                          (org-element-property :priority headline)))
           (text (org-export-data (org-element-property :title headline) info))
           (tags (and (plist-get info :with-tags)
                      (org-export-get-tags headline info)))
           (full-text (funcall (plist-get info :html-format-headline-function)
                               todo todo-type priority text tags info))
           (contents (or contents ""))
           (id (org-html--reference headline info))
           (formatted-text
            (if (plist-get info :html-self-link-headlines)
                (format "<a href=\"#%s\">%s</a>" id full-text)
              full-text)))
      (if (org-export-low-level-p headline info)
          ;; This is a deep sub-tree: export it as a list item.
          (let* ((html-type (if numberedp "ol" "ul")))
            (concat
             (and (org-export-first-sibling-p headline info)
                  (apply #'format "<%s class=\"org-%s\">\n"
                         (make-list 2 html-type)))
             (org-html-format-list-item
              contents (if numberedp 'ordered 'unordered)
              nil info nil
              (concat (org-html--anchor id nil nil info) formatted-text)) "\n"
             (and (org-export-last-sibling-p headline info)
                  (format "</%s>\n" html-type))))
        ;; Standard headline.  Export it as a section.
        (let ((extra-class
               (org-element-property :HTML_CONTAINER_CLASS headline))
              (headline-class
               (org-element-property :HTML_HEADLINE_CLASS headline))
              (first-content (car (org-element-contents headline))))
          (format "%s<%s id=\"%s\" class=\"%s collapsme\">\n%s</%s>\n"
                  (format "<h%d class=\"collapsible\" id=\"%s\"%s>%s</h%d>\n"
                          level
                          id
                          (if (not headline-class) ""
                            (format " class=\"%s\"" headline-class))
                          (concat
                           (and numberedp
                                (format
                                 "<span class=\"section-number-%d\">%s</span> "
                                 level
                                 (concat (mapconcat #'number-to-string numbers ".") ".")))
                           formatted-text)
                          level)
                  (org-html--container headline info)
                  (format "outline-container-%s" id)
                  (concat (format "outline-%d" level)
                          (and extra-class " ")
                          extra-class)
                  ;; When there is no section, pretend there is an
                  ;; empty one to get the correct <div
                  ;; class="outline-...> which is needed by
                  ;; `org-info.js'.
                  (if (eq (org-element-type first-content) 'section) contents
                    (concat (org-html-section first-content "" info) contents))
                  (org-html--container headline info)))))))

(require 'german-holidays)
(setq calendar-holidays holiday-german-NI-holidays)

(setq calendar-date-style 'iso)
(setq calendar-day-abbrev-array ["So" "Mo" "Di" "Mi" "Do" "Fr" "Sa"])
(setq calendar-day-header-array ["So" "Mo" "Di" "Mi" "Do" "Fr" "Sa"])
(setq calendar-day-name-array
       ["Sonntag" "Montag" "Dienstag" "Mittwoch" "Donnerstag" "Freitag" "Samstag"])
(setq calendar-iso-date-display-form
       '((format "%s-%.2d-%.2d" year
			 (string-to-number month)
			 (string-to-number day))))
(setq calendar-iso-month-header
       '(propertize
	 (format "%d-%s" year
			 (calendar-month-name month))
	 'font-lock-face 'calendar-month-header))
(setq calendar-mark-diary-entries-flag t)
(setq calendar-month-name-array
       ["Januar" "Februar" "M\303\244rz" "April" "Mai" "Juni" "Juli" "August" "September" "Oktober" "November" "Dezember"])
(setq calendar-standard-time-zone-name "CEST")
(setq calendar-time-display-form
       '(24-hours ":" minutes
			      (if time-zone " (")
			      time-zone
			      (if time-zone ")")))
(setq calendar-view-diary-initially-flag t)
(setq calendar-week-start-day 1)
(setq plstore-cache-passphrase-for-symmetric-encrytion t)

(require 'calibredb)
(setq calibredb-root-dir "~/data/ebooks/calibre")
(setq calibredb-db-dir (expand-file-name "metadata.db" calibredb-root-dir))
(setq calibredb-library-alist '(("~/data/ebooks/calibre")))
(setq calibredb-id-width 5)

(add-hook 'diary-list-entries-hook 'diary-sort-entries t)
(add-hook 'diary-list-entries-hook 'diary-include-other-diary-files)
(add-hook 'diary-mark-entries-hook 'diary-mark-included-diary-files)

(require 'wdired)
(setq dired-listing-switches "-hl --group-directories-first")
(setq dired-omit-files "^\\.?#\\|^\\.$\\|^\\.\\.$\\|^\\..*$")
(setq dired-use-ls-dired t)

(require 'eww)
(setq eww-search-prefix "https://duckduckgo.com/?kae=b&kl=de-de&kad=de_DE&kp=-1&kw=w&kak=-1&kah=de-de&kn=-1&kaj=m&kam=osm&kv=-1&kao=-1&kd=-1&kc=-1&kac=-1&k1=-1&kk=-1&kz=-1&q=")

(require 'elfeed)
(require 'elfeed-org)
(setq rmh-elfeed-org-files (list "~/org/feeds.org"))

(add-hook 'python-mode-hook 'hs-minor-mode)

(require 'nov)
(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))
(setq nov-text-width t)
(add-hook 'nov-mode-hook 'visual-line-mode)
(add-hook 'nov-mode-hook 'visual-fill-column-mode)

(setq erc-autojoin-mode t)
(setq erc-autojoin-timing 'ident)
(setq erc-button-mode t)
(setq erc-fill-mode t)
(setq erc-hide-list '("QUIT"))
(setq erc-irccontrols-mode t)
(setq erc-join-buffer 'buffer)
(setq erc-list-mode t)
(setq erc-match-mode t)
(setq erc-menu-mode t)
(setq erc-modules
 '(autojoin button completion fill irccontrols list match menu move-to-prompt netsplit networks noncommands readonly ring services stamp))
(setq erc-move-to-prompt-mode t)
(setq erc-netsplit-mode t)
(setq erc-networks-alist
   '((4-irc "4-irc.com")
	 (A5KNet "a5knet.com")
	 (Zurna "zurna.net")))
(setq erc-networks-mode t)
(setq erc-nick "DrOps")
(setq erc-nick-uniquifier "s")
(setq erc-noncommands-mode t)
(setq erc-pcomplete-mode t)
(setq erc-prompt-for-nickserv-password nil)
(setq erc-prompt-for-password nil)
(setq erc-readonly-mode t)
(setq erc-ring-mode t)
(setq erc-server "irc.libera.chat")
(setq erc-server-alist
   '(("4-irc: Random server" 4-irc "4-irc.com" 6667)
	 ("A5KNet: Random server" A5KNet "irc.a5knet.com"
	  ((6660 6669)))
	 ("Zurna: Random server" Zurna "irc.zurna.net" 6667)))
(setq erc-services-mode t)
(setq erc-speedbar-sort-users-type 'alphabetical)
(setq erc-stamp-mode t)
(setq erc-system-name "plaindrops.de")
(setq erc-track-minor-mode t)
(setq erc-track-mode t)
(setq erc-user-full-name "Andy Drop")

(require 'compile)
(add-to-list 'compilation-error-regexp-alist-alist '(ledger "\"\\([A-Za-z0-9\\./]+\\)\", line \\([0-9]+\\)" 1 2))
(add-to-list 'compilation-error-regexp-alist 'ledger)


  (require 'ledger-mode)
  (setq ledger-default-date-format "%Y-%m-%d")
  (setq ledger-schedule-file "~/fin/schedule.ledger")
  (setq ledger-accounts-file "~/fin/accounts.ledger")
  (setq ledger-clear-whole-transactions t)

  (setq ledger-post-account-alignment-column 2)
  (setq ledger-post-amount-alignment-at :decimal)
  (setq ledger-post-amount-alignment-column 58)

  (setq ledger-reconcile-default-date-format "%Y-%m-%d")
  (setq ledger-reconcile-default-commodity " ")

  (setq ledger-report-auto-width t)
  (setq ledger-report-use-strict t)

  (add-hook 'ledger-mode-hook
	    (lambda ()
	      (setq-local tab-always-indent 'complete)
	      (setq-local completion-cycle-threshold t)
	      (setq-local ledger-complete-in-steps t)))

  (setq ledger-schedule-look-forward 7)
  (setq ledger-schedule-week-days
	'(("Mo" 1)
	  ("Di" 2)
	  ("Mi" 3)
	  ("Do" 4)
	  ("Fr" 5)
	  ("Sa" 6)
	  ("So" 0)))
  (setq ledger-reports
	'(("Hauszahlungen" "ledger reg Hauszahlung")
	  ("Bestand Bausparen" "ledger bal Bausparen:")
	  ("Bausparkasse M07 Sparkonto" "ledger reg M07h")
	  ("Benzin" "ledger reg Budget:Benzin")
	  ("Bestand" "ledger bal --depth 2 Haben Soll")
	  ("Bestand Bargeld" "ledger [[ledger-mode-flags]] -f %(ledger-file) bal Bargeld")
	  ("Bestand Giro" "ledger bal giro")
	  ("Bestand Kreditkarte" "ledger [[ledger-mode-flags]] -f %(ledger-file) bal Kreditkarte")
	  ("Budget Leben M07" "ledger reg \"Budget:A:Leben M07\"")
	  ("Equity Report" "ledger equity")
	  ("Freies Gehalt" "ledger [[ledger-mode-flags]] -f ~/fin/drops.ledger reg Liquid and @Firma")
	  ("Privatschulden" "ledger reg Privat")
	  ("Steuer- Telefonrechnungen" "ledger reg -p \"last year\" Telefon:Festnetz")
	  ("Steuer-Haftpflicht" "ledger reg -p \"last year\" Versicherungen:Haftpflicht")
	  ("Steuer-KFZ-Haftpflicht" "ledger reg -p \"last year\" Ausgaben:Mobilität:Auto:Versicherungen")
	  ("Steuer-Kaminkehrer" "ledger reg -p \"last year\" Kaminkehrer")
	  ("Steuer-Lebensversicherung" "ledger reg -p \"last year\" Ausgaben:Versicherungen:Leben")
	  ("Steuer-Unfallversicherung" "ledger reg -p \"last year\" Ausgaben:Versicherungen:Unfall")
	  ("Steuer-Versicherungen" "ledger reg -p \"last year\" Telefon:Festnetz")
	  ("Steuern-Rente" "ledger reg -p \"last year\" Einnahmen:Rente:Andy")
	  ("Verlauf Barclay" "ledger reg Kreditkarte")
	  ("Verlauf Bargeld" "ledger [[ledger-mode-flags]] -f %(ledger-file) reg Bargeld")
	  ("Verlauf Bausparen" "ledger reg Bausparen:M0")
	  ("Verlauf Gas" "ledger reg Budget:A:Gas")
	  ("Verlauf Girokonto" "ledger reg Giro")
	  ("Verlauf Haftpflicht" "ledger reg Budget:A:Haftpflicht")
	  ("Verlauf Kreditkarte" "ledger [[ledger-mode-flags]] -f %(ledger-file) reg Kreditkarte")
	  ("Verlauf M07" "ledger reg Bausparen:M07")
	  ("Verlauf M07h" "ledger reg Haben:Bausparen:M07")
	  ("Verlauf M08" "ledger reg Bausparen:M08")
	  ("Verlauf M08h" "ledger reg Haben:Bausparen:M08")
	  ("Verlauf M08s" "ledger reg Soll:Bausparen:M08")
	  ("Verlauf M09" "ledger reg Bausparen:M09")
	  ("Verlauf M09h" "ledger reg Haben:Bausparen:M09")
	  ("Verlauf Zinsen" "ledger reg Ausgaben:Finanzen:Zinsen")
	  ("Verlauf" "ledger reg Haben Soll")
	  ("Verlauf-Barclaycard" "ledger [[ledger-mode-flags]] -f ~/fin/drops.ledger reg Soll:Kreditkarte")
	  ("Verlauf-Bargeld" "ledger [[ledger-mode-flags]] -f %(ledger-file) reg Bargeld")
	  ("account" "%(binary) -f %(ledger-file) reg %(account)")
	  ("budget" "%(binary) budget -f %(ledger-file) ")
	  ("payee" "%(binary) -f %(ledger-file) reg @%(payee)")
	  ("reg Budget:Leben" "ledger reg Budget:Leben ")
	  ("reg M07" "ledger reg M07h")
	  ("reg reminder" "ledger reg reminder")))

(add-to-list 'load-path "/home/andy/.config/emacs/lisp/mastodon.el/lisp")
   (require 'mastodon)
 (setq mastodon-instance-url "https://chaos.social"
	     mastodon-active-user "Drops")

;; Check if system is Microsoft Windows
(defun my-system-type-is-windows ()
      "Return true if system is Windows-based (at least up to Win7)"
      (string-equal system-type "windows-nt")
      )

;; Check if system is GNU/Linux
(defun my-system-type-is-linux ()
      "Return true if system is GNU/Linux-based"
      (string-equal system-type "gnu/linux")
      )

(setq smtpmail-debug-info t)
(setq smtpmail-default-smtp-server "smtp.mailbox.org")
(setq smtpmail-queue-dir "~/mail/queued-mail/")
(setq smtpmail-smtp-server "smtp.mailbox.org")
(setq smtpmail-smtp-service 465)
(setq smtpmail-stream-type 'ssl)

(setq org-agenda-category-icon-alist '(("todo" "org/icons/todo16.png" nil nil :ascent\ center)))
(setq org-agenda-custom-commands
   '(("n" "Agenda and all TODOs"
	  ((tags "GTD=\"t\"+TODO=\"NEXT\""
			 ((org-agenda-overriding-header "Next Actions")))
	   (stuck "" nil)
	   (tags-todo "GTD<>\"t\"+CATEGORY<>\"Haushalt\""
				  ((org-agenda-overriding-header "Sonstige ToDo")))
	   (tags-todo "CATEGORY=\"Haushalt\"+SCHEDULED<=\"<+2d>\""
				  ((org-agenda-overriding-header "Haushalt")))
	   (agenda "" nil))
	  nil
	   ("~/org/agenda.html" "~/org/agenda.txt"))))
(setq org-agenda-files
   '("~/org/ops.org" "~/org/birthday-calendar.org" "~/org/calendar.org" "~/org/Haushalt.org"))
(setq org-agenda-include-diary t)
(setq org-agenda-skip-deadline-prewarning-if-scheduled 1)
(setq org-agenda-skip-scheduled-if-deadline-is-shown 'not-today)
(setq org-agenda-skip-scheduled-if-done t)
(setq org-agenda-skip-timestamp-if-deadline-is-shown t)
(setq org-agenda-skip-timestamp-if-done t)
(setq org-agenda-span 'fortnight)
(setq org-agenda-time-grid
   '((daily today)
	 (800 1000 1200 1400 1600 1800 2000)
	 "......" "----------------"))
(setq org-agenda-time-leading-zero t)
(setq org-agenda-window-setup 'reorganize-frame)
(org-babel-do-load-languages
 'org-babel-load-languages
 '((R . t)
   (emacs-lisp . t)
   (gnuplot . t)
   (python . t)
)
)
 (setq org-babel-load-languages '((emacs-lisp . t) (ledger . t) (plantuml . t)))
 (setq org-babel-python-command "python3")
 (setq org-caldav-calendar-id "personal")
 (setq org-caldav-calendars '((:calendar-id "personal" :inbox "~/org/calendar.org")))
 (setq org-caldav-files '("~/org/calendar.org" "~/org/ops.org"))
 (setq org-caldav-inbox "~/org/calendar.org")
 (setq org-caldav-show-sync-results nil)
 (setq org-caldav-sync-changes-to-org 'all)
 (setq org-caldav-sync-direction 'twoway)
 (setq org-caldav-url
	"https://nextcloud.plaindrops.de/remote.php/dav/calendars/andy")

 (setq org-closed-keep-when-no-todo t)
 (setq org-crypt-disable-auto-save t)
 (setq org-deadline-past-days 21)
 (setq org-deadline-warning-days 1)
 (setq org-directory "~/org")
 (setq org-download-screenshot-method "scrot -s %s")
 (setq org-edna-mode t)
 (setq org-edna-use-inheritance t)
 (setq org-ellipsis nil)
 (setq org-enforce-todo-dependencies t)
 (setq org-export-use-babel nil)
 (setq org-hide-block-startup t)
 (setq org-hide-emphasis-markers t)
 (setq org-hide-leading-stars t)
 (setq org-html-html5-fancy t)
 (setq org-html-toplevel-hlevel 3)
 (setq org-icalendar-combined-agenda-file "/tmp/org-caldav-GvSoW8")
 (setq org-icalendar-include-todo 'all)
 (setq org-icalendar-store-UID t)
 (setq org-icalendar-timezone "Europe/Berlin")
 (setq org-icalendar-use-deadline '(todo-due))
 (setq org-icalendar-use-scheduled '(event-if-not-todo todo-start))
 (setq org-image-actual-width 640)
 (setq org-journal-date-format "%Y-%m-%d (%A)")
 (setq org-journal-dir "~/org/journal/")
 (setq org-journal-file-format "Journal-%Y")
 (setq org-link-frame-setup
	'((vm . vm-visit-folder-other-frame)
	  (vm-imap . vm-visit-imap-folder-other-frame)
	  (gnus . org-gnus-no-new-news)
	  (file . find-file)
	  (wl . wl-other-frame)))
 (setq org-link-from-user-regexp "\\<andy\\>")
 (setq org-log-done 'time)
 (setq org-plantuml-jar-path "/usr/share/plantuml/plantuml.jar")
 (setq org-refile-allow-creating-parent-nodes 'confirm)
 (setq org-refile-targets '(("links.org" :maxlevel . 3)))
 (setq org-refile-use-outline-path nil)
 (setq org-reveal-root "file://~/bin/reveal.js")
 (setq org-reveal-theme "solarized")
 (setq org-src-block-faces '(("*" fixed-pitch)))

(defun drops/link-heading-with-own-id ()
  "Replace an Org mode heading at point
   with a linked version using its own ID,
   preserving the heading level
   and creating one if it does not already exist."
  (interactive)
  (org-back-to-heading)
  (let* ((heading-level (org-outline-level))
	 (heading-start (point))
	 (heading-end (line-end-position))
	 (heading (nth 4 (org-heading-components)))
	 (id (org-entry-get nil "CUSTOM_ID")))
    (if (not id)
	(progn
	  (setq id (concat "id-" (md5 heading)))
	  (org-set-property "CUSTOM_ID" id)))
    (delete-region heading-start heading-end)
    (insert (format "%s [[#%s][%s]]"
		    (make-string heading-level ?*)
		    id heading)))
  )

(setq org-agenda-files
 '("~/org/ops.org" "~/org/birthday-calendar.org" "~/org/calendar.org" "~/org/Haushalt.org" ))
(setq org-agenda-include-diary t)
(setq org-agenda-loop-over-headlines-in-active-region nil)
(setq org-agenda-restore-windows-after-quit t)
(setq org-agenda-show-future-repeats nil)
(setq org-agenda-skip-deadline-prewarning-if-scheduled t)
(setq org-agenda-skip-scheduled-if-deadline-is-shown 'not-today)
(setq org-agenda-skip-scheduled-if-done t)
(setq org-agenda-skip-timestamp-if-deadline-is-shown t)
(setq org-agenda-skip-timestamp-if-done t)
(setq org-agenda-span 10)
(setq org-agenda-start-on-weekday nil)
(setq org-agenda-tags-todo-honor-ignore-options t)
(setq org-agenda-time-leading-zero t)
(setq org-agenda-todo-ignore-scheduled 'future)
(setq org-agenda-window-setup 'current-window)
 (setq org-agenda-custom-commands
      '(("n" "Agenda and all TODOs"
	 ((tags "GTD=\"t\"+TODO=\"NEXT\""
			((org-agenda-overriding-header "Next Actions")))
	      (stuck "" nil)
	      (tags-todo "GTD<>\"t\"+CATEGORY<>\"Haushalt\""
				 ((org-agenda-overriding-header "Sonstige ToDo")))
	      (tags-todo "CATEGORY=\"Haushalt\"+SCHEDULED<=\"<+2d>\""
				 ((org-agenda-overriding-header "Haushalt")))
	      (agenda "" nil))
	 nil
	 ("~/org/agenda.txt" "~/org/agenda.html"))))

(setq org-icalendar-combined-agenda-file "/tmp/org-caldav-wtBkYE")
(setq org-outline-path-complete-in-steps t)

;; load gnuplot mode
(require 'gnuplot)
(require 'ob-gnuplot)

(require 'org-protocol)
  (setq org-capture-templates
  '(("g" "Gutschein" table-line
			     (file+headline "~/org/nummern.org" "Gutscheine")
			     "| %? | Thema | Nummer | 10\342\202\254 | Mindestbestellwert | 2020-12-31 |")
("Q" "Quotelink" entry
(file+headline "~/wiki/20221010T232442--zitate__quotes.org" "Inbox")
"** %i
:PROPERTIES:
:TITLE: %:description:
:URL: %:link
:SAVED: %<%Y-%m-%d>
:END:

#+BEGIN_QUOTE
%i
[[%:link][%:description]]
#+END_QUOTE

" :immediate-finish t)
("L" "Weblink" entry
(file+headline "~/org/links.org" "Inbox")
"* [[%:link][%:description]]
:PROPERTIES:
:Bookmarked: %<%Y-%m>
:END: " :immediate-finish t)
("jj" "Dayly Log" plain
 (file+olp+datetree "~/jrn/log/daily.org.gpg" "Log")
 "     %?" :empty-lines 1 :time-prompt t)
("jp" "Period Log" entry
 (file "~/jrn/log/period.org.gpg")
 "* ")
("f" "Finanzen in Ledger" plain
 (file "~/org/finanzen.ledger")
 "%(shell-command-to-string (format \"ledger xact %s %s\" (substring \"%:date\" 1 11) \"%:description\"))                                    " :immediate-finish t :empty-lines 1)
("p" "Project" entry
 (file+headline "~/org/ops.org" "Projects")
 "* [/] %?%:description :project:")
("t" "ToDo" entry
 (file+headline "~/org/ops.org" "--- ToDo's ---")
 "* %?" :empty-lines 1)
("j" "Journal")
("" "" entry
 (file "~/org/notes.org")
 "")
("js" "Stepping Stones" entry
 (file "~/jrn/log/steppingstones.org.gpg")
 "")
("ji" "Intersections" entry
 (file "~/jrn/log/intersections.org.gpg")
 "")
("jn" "Now: The Open " entry
 (file "~/jrn/log/now.org.gpg")
 "")
("jd" "Dreams Log" entry
 (file+olp+datetree "~/jrn/depth/dreams.org.gpg" "Log")
 "")
("jt" "Twilight Imagery" entry
 (file+olp+datetree "~/jrn/depth/twilight.org.gpg" "Log")
 "")
("jD" "Dreams extension" entry
 (file "~/jrn/depth/dreams-extended.org.gpg")
 "* ")
("jT" "Twighlight enhancements" entry
 (file "~/jrn/depth/twilight-enhanced.org.gpg")
 "* ")
("b" "Barclay Buchung" plain
(file "~/fin/drops.ledger")
"%<%Y-%m-%d>   %?
  Ausgaben:Haushalt:Sonstiges                            %x
  Soll:Kreditkarte                                      -%x
  Budget:Kreditkarte                                     %x
  Giro
                                   
" :empty-lines 1)
))

(require 'org-crypt)
;; GPG key to use for encryption
;; Either the Key ID or set to nil to use symmetric encryption.
(setq org-crypt-key "CABFD3324FD3279F63070228F58A421AE336FFBD")
(setq epa-pinentry-mode 'loopback)

(org-crypt-use-before-save-magic)

(setq org-tags-exclude-from-inheritance (quote ("crypt")))

(setq org-crypt-disable-auto-save t)

(require 'org-download)

;; Drag-and-drop to `dired`
(add-hook 'dired-mode-hook 'org-download-enable)

(setq org-download-abbreviate-filename-function 'expand-file-name)
(setq org-download-heading-lvl nil)
(setq org-download-image-attr-list '(""))
(setq org-download-image-dir "~/wiki/images")
(setq org-download-image-html-width 640)
(setq org-download-image-org-width 320)
(setq org-download-screenshot-method "scrot -s %s")

(setq org-edna-mode t)
(setq org-edna-use-inheritance t)
(setq org-ellipsis " ▼")
(setq org-enforce-todo-dependencies t)
(setq org-export-use-babel nil)
(setq org-export-with-toc 2)
(setq org-hide-block-startup t)
(setq org-hide-emphasis-markers t)
(setq org-hide-leading-stars t)
(setq org-html-html5-fancy t)
(setq org-html-toplevel-hlevel 3)
(setq org-icalendar-combined-agenda-file "/tmp/org-caldav-wtBkYE")
(setq org-icalendar-include-todo 'all)
(setq org-icalendar-store-UID t)
(setq org-icalendar-timezone "Europe/Berlin")
(setq org-image-actual-width 480)
(setq org-indirect-buffer-display 'current-window)
(setq org-link-from-user-regexp "\\<andy\\>")
(setq org-log-done 'time)
(setq org-refile-allow-creating-parent-nodes 'confirm)
(setq org-refile-targets
       '(("~/org/rezepte.org" :maxlevel . 2)
	 ("~/org/links.org" :maxlevel . 4)
	 ("~/org/ops.org" :level . 1)
	 ("~/org/lyrics.org" :level . 1)))
(setq org-refile-use-outline-path nil)
(setq org-reveal-root "file://~/bin/reveal.js")
(setq org-reveal-theme "solarized")
(setq org-roam-db-autosync-mode t)
(setq org-roam-directory "/home/andy/wiki")
(setq org-src-block-faces 'nil)
(setq org-support-shift-select 'always)
(setq org-tags-column -64)
(setq org-todo-keywords
       '((sequence "proj(p)" "TODO(t)" "wait(w)" "NEXT(n)" "|" "DONE(d)" "stop(s)" "forw(f)")))
(setq org-todo-repeat-to-state "NEXT")
(setq org-use-property-inheritance '("GTD"))
(setq org-use-speed-commands t)

(load (concat user-emacs-directory "lisp/org-geolink.el"))
(require 'org-geolink)

(setq org-crypt-disable-auto-save t)

(eval-after-load 'org '(require 'org-pdfview))

(add-to-list 'org-file-apps 
             '("\\.pdf\\'" . (lambda (file link)
                                     (org-pdfview-open link))))

(setq org-refile-allow-creating-parent-nodes 'confirm)
(setq org-refile-targets
 '(("~/org/rezepte.org" :maxlevel . 2)
	 ("~/org/links.org" :maxlevel . 4)
	 ("~/org/ops.org" :level . 1)
	 ("~/org/lyrics.org" :level . 1)
	 ;;(ndk/org-refile-candidates :maxlevel . 3)
	 ))
(setq org-refile-use-cache t)
(setq org-refile-use-outline-path nil)

(setq org-startup-align-all-tables t)
 (setq org-startup-folded 'content)
 (setq org-startup-with-inline-images t)

 (setq org-stuck-projects '("Project=\"t\"" ("NEXT") nil ""))
 (setq org-support-shift-select t)
 (setq org-tags-column -64)
 (setq org-tags-exclude-from-inheritance '("crypt"))
 (setq org-todo-keywords   '((sequence "proj(p)" "TODO(t)" "wait(w)" "NEXT(n)" "|" "DONE(d)" "stop(s)" "forw(f)")))
 (setq org-todo-repeat-to-state "NEXT")
 (setq org-use-property-inheritance '("GTD"))
(setq org-babel-python-command "python3")
(setq org-blank-before-new-entry '((heading . t) (plain-list-item . t)))
(setq org-closed-keep-when-no-todo t)
(setq org-confirm-babel-evaluate nil)
(setq org-deadline-past-days 21)
(setq org-deadline-warning-days 1)

(load (concat user-emacs-directory "lisp/org-yt.el"))
(require 'org-yt)

;;(require 'org-special-block-extras)

(require 'treemacs)
(setq treemacs-collapse-dirs 3)
(setq treemacs-filewatch-mode t)
(setq treemacs-follow-mode t)
(setq treemacs-fringe-indicator-mode t)
(setq treemacs-git-mode t)
(setq treemacs-project-follow-cleanup t)
(setq treemacs-select-when-already-in-treemacs 'next-or-back)

(add-hook 'text-mode-hook #'abbrev-mode)

(require 'afterglow)
(afterglow-mode 1)

;; Optional
(setq afterglow-default-duration 0.5)
(setq afterglow-default-face 'hl-line)

;; Example 1:
(afterglow-add-triggers
 '((evil-previous-visual-line :thing line :width 5 :duration 0.2)
   (evil-next-visual-line :thing line :width 5 :duration 0.2)
   (previous-line :thing line :duration 0.2)
   (next-line :thing line :duration 0.2)
   (eval-buffer :thing window :duration 0.2)
   (eval-defun :thing defun :duration 0.2)
   (eval-expression :thing sexp :duration 1)
   (eval-last-sexp :thing sexp :duration 1)
   (my-function :thing my-region-function :duration 0.5 
                :face 'highlight)))

(require 'corral)

(setq corral-preserve-point t)

(load (concat user-emacs-directory "lisp/dame.el"))
(load (concat user-emacs-directory "lisp/dame-org-rifle.el"))
(load (concat user-emacs-directory "lisp/dame-org-ql.el"))

(require 'org-edna)
(setq orgstuck-keywords '("project" ("NEXT") nil ""))

(setq Org-todo-repeat-to-state "NEXT")

(org-edna-mode)



(setq flymake-error-bitmap '(flymake-double-exclamation-mark modus-themes-fringe-red))
(setq flymake-note-bitmap '(exclamation-mark modus-themes-fringe-cyan))
(setq flymake-warning-bitmap '(exclamation-mark modus-themes-fringe-yellow))

(require 'golden-ratio)
(golden-ratio-mode 1)

(require 'marginalia)
(marginalia-mode)

(require 'consult)

(require 'orderless)
(setq completion-styles '(substring orderless)
      completion-category-defaults nil
      completion-category-overrides '((file (styles partial-completion))))

(setq Linum-format "%7i ")
(setq after-save-hook '(org-babel-tangle))
;;(setq ansi-color-faces-vector [default bold shadow italic underline success warning error])
;;(setq ansi-color-map '((ansi-color-make-color-map) t))
;;(setq ansi-color-names-vector ["#454545" "#d65946" "#6aaf50" "#baba36" "#598bc1" "#ab75c3" "#68a5e9" "#AAB0AB"])
(setq auto-revert-avoid-polling t)
(setq awesome-tray-mode-line-active-color "#2fafff")
(setq awesome-tray-mode-line-inactive-color "#323232")
(setq backup-directory-alist '(("." . "~/.backup")))
(setq beacon-color "#ed0547ad8099")
(setq blink-cursor-mode nil)
;;(setq bmkp-last-as-first-bookmark-file concat)
(setq browse-url-firefox-new-window-is-tab t)
(setq browse-url-firefox-program "/home/andy/.local/bin/firefox")
(setq cal-tex-which-days '(1 2 3 4 5 6 0))
(setq calendar-mark-diary-entries-flag t)
(setq calendar-month-name-array
 ["Januar" "Februar" "März" "April" "Mai" "Juni" "Juli" "August" "September" "Oktober" "November" "Dezember"])
(setq calendar-view-diary-initially-flag t)
(setq column-number-mode t)
(setq company-quickhelp-color-background "#4F4F4F")
(setq company-quickhelp-color-foreground "#DCDCCC")
(setq compilation-message-face 'default)
(setq completion-styles '(substring orderless))
(setq confirm-kill-processes nil)
(setq consult-preview-key '(:debounce 0.5 any))
(setq create-lockfiles nil)
(setq cua-auto-tabify-rectangles nil)
(setq cua-mode t)
(setq cua-normal-cursor-color "black")
(setq custom-buffer-style 'link)
(setq custom-enabled-themes '(DrOps))
(setq custom-file "~/.config/emacs/custom.el")
(setq custom-safe-themes
       '("fe497072cd9ff25d187db65196b2910d78c938ed71bd3991163f3da6fda62757" "f3c9e341d20be3c006cc8bccb309ef439083d47fed664b4a23133e67b1f8cab8" "92f458ebdf4a4a84f7ff089d58d8c097f09dbf5a4870151a02d257a8574e8671" "043eacb4b2b51cdce979f24b38407e2c6a8f199bafd134e4b9002106ea96c6ad" default))
(setq default-input-method "german-postfix")
(setq diary-date-forms
 '((month "-" day "[^-0-9]")
	 (year "[-/]" month "[-/]" day "[^0-9]")
	 (dayname "\\W")))
(setq diary-file "~/org/diary")
(setq diary-number-of-entries 7)
(setq dictcc-destination-lang "en")
(setq dictcc-languages-alist
 '(("English" . "en")
	 ("German" . "de")
	 ("Swedish" . "sv")
	 ("Icelandic" . "is")
	 ("Russian" . "u")
	 ("Romanian" . "ro")
	 ("Italian" . "it")
	 ("French" . "fr")
	 ("Portuguese" . "pt")
	 ("Hungarian" . "hu")
	 ("Dutch" . "nl")
	 ("Slovak" . "sk")
	 ("Latin" . "la")
	 ("Finnish" . "fi")
	 ("Spanish" . "es")
	 ("Bulgarian" . "bg")
	 ("Croation" . "hr")
	 ("Norwegian" . "no")
	 ("Czech" . "cs")
	 ("Danish" . "da")
	 ("Turkish" . "tr")
	 ("Polish" . "pl")
	 ("Serbian" . "sr")
	 ("Greek" . "el")
	 ("Esperanto" . "eo")
	 ("Bosnian" . "bs")
	 ("Albanian" . "sq")))
(setq dictcc-source-lang "de")
(setq diff-hl-show-hunk-posframe-internal-border-color "#357535753575")
(setq dired-auto-revert-buffer t)
(setq dired-do-revert-buffer '(lambda (dir) (not (file-remote-p dir))))
(setq dired-kill-when-opening-new-dired-buffer t)
(setq display-time-use-mail-icon t)
(setq dnd-open-file-other-window t)

(setq ediff-merge-split-window-function 'split-window-horizontally)
(setq ediff-split-window-function 'split-window-horizontally)
(setq ediff-use-last-dir t)
(setq ediff-window-setup-function 'ediff-setup-windows-default)

(setq electric-pair-mode t)

(setq elfeed-goodies/entry-pane-size 0.5)
(setq elfeed-goodies/log-window-position 'right)

(setq emacsshot-with-timestamp t)
(setq eww-search-prefix
"https://duckduckgo.com/?kae=b&kl=de-de&kad=de_DE&kp=-1&kw=w&kak=-1&kah=de-de&kn=-1&kaj=m&kam=osm&kv=-1&kao=-1&kd=-1&kc=-1&kac=-1&k1=-1&kk=-1&kz=-1&q=")
(setq exwm-floating-border-color "#646464")
(setq fci-rule-character-color "#202020")
(setq fci-rule-color "#222222")
(setq fill-column 100)

(setq folding-mode-string " fold")
(setq font-lock-global-modes '(not speedbar-mode))
(setq frame-background-mode 'dark)
(setq fringe-mode 4 )
(setq global-auto-revert-mode t)
(setq global-reveal-mode nil)
(global-visual-line-mode 1)
(setq fill-column 100)
(setq gnus-group-update-tool-bar t)
(setq go-translate-local-language "de")

(setq highlight-changes-colors '("#ff8eff" "#ab7eff"))
(setq highlight-indent-guides-auto-enabled nil)
(setq highlight-symbol-colors
 '("#FFEE58" "#C5E1A5" "#80DEEA" "#64B5F6" "#E1BEE7" "#FFCC80"))
(setq highlight-symbol-foreground-color "#E0E0E0")
(setq highlight-tail-colors
 '(("#323342" . 0)
	 ("#63de5d" . 20)
	 ("#4BBEAE" . 30)
	 ("#1DB4D0" . 50)
	 ("#9A8F21" . 60)
	 ("#A75B00" . 70)
	 ("#F309DF" . 85)
	 ("#323342" . 100)))
(setq hl-todo-keyword-faces
 '(("HOLD" . "#c0c530")
	 ("TODO" . "#feacd0")
	 ("NEXT" . "#b6a0ff")
	 ("THEM" . "#f78fe7")
	 ("PROG" . "#00d3d0")
	 ("OKAY" . "#4ae2f0")
	 ("DONT" . "#70b900")
	 ("FAIL" . "#ff8059")
	 ("BUG" . "#ff8059")
	 ("DONE" . "#44bc44")
	 ("NOTE" . "#d3b55f")
	 ("KLUDGE" . "#d0bc00")
	 ("HACK" . "#d0bc00")
	 ("TEMP" . "#ffcccc")
	 ("FIXME" . "#ff9077")
	 ("XXX+" . "#ef8b50")
	 ("REVIEW" . "#6ae4b9")
	 ("DEPRECATED" . "#bfd9ff")))
(setq hydra-hint-display-type 'posframe)

(setq ibuffer-deletion-face 'diredp-deletion-file-name)
(setq ibuffer-filter-group-name-face 'modus-themes-pseudo-header)
(setq ibuffer-marked-face 'diredp-flag-mark)
(setq ibuffer-title-face 'default)

(setq inhibit-startup-screen t)

(setq keypression-mode t)
(setq keypression-use-child-frame t)

(setq ledger-default-date-format "%Y-%m-%d")
(setq ledger-reconcile-default-commodity " ")

(setq lsp-ui-imenu-colors '("#7FC1CA" "#A8CE93"))
(setq lunar-phase-names
 '("Neumond" "zunehmender Mond" "Vollmand" "abnehmender Mond"))

(setq magit-auto-revert-mode t)
(setq magit-diff-use-overlays nil)

(setq mail-user-agent 'mu4e-user-agent)
(setq main-line-color1 "#1E1E1E")
(setq main-line-color2 "#111111")
(setq main-line-separator-style 'chamfer)
(setq major-mode-icons-mode nil)
(setq menu-bar-mode nil)
(setq mlscroll-in-color "#56bc56bc56bc")
(setq mlscroll-out-color "#424242")
(setq mm-inline-large-images 'resize)
(setq mml-secure-passphrase-cache-expiry 16)
(setq mode-icons-mode t)

(setq nov-text-width 80)
(setq nrepl-message-colors
 '("#CC9393" "#DFAF8F" "#F0DFAF" "#7F9F7F" "#BFEBBF" "#93E0E3" "#94BFF3" "#DC8CC3"))
(setq omnisharp-auto-complete-popup-help-delay 2000)
(setq openwith-associations
 '(("\\.\\(?:m\\(?:aff\\|ht\\(?:ml\\)?\\)\\)$" "qutebrowser"
	      (file))
	 ("\\.\\(?:flac\\|m\\(?:4a\\|p3\\)\\|wav\\|webm\\)$" "vlc"
	      (file))
	 ("\\.\\(?:avi\\|flv\\|m\\(?:ov\\|p\\(?:eg\\|[4g]\\)\\)\\|ogg\\|wmv\\)$" "vlc"
	      (file))
	 ("\\.\\(?:docx?\\|odt\\)$" "libreoffice"
	      ("--writer" file))
	 ("\\.\\(?:ods\\|xlsx?\\)$" "libreoffice"
	      ("--calc" file))
	 ("\\.\\(?:odp\\|pp\\(?:tx\\|[st]\\)\\)$" "libreoffice"
	      ("--impress" file))
	 ))
(setq openwith-mode t)

(setq package-selected-packages
 '(openwith runner sudo-edit unify-opening mastodon ibuffer-projectile calibredb nov vertico-posframe major-mode-hydra projectile denote org-link-beautify org-tag-beautify osm company-ledger elfeed-goodies consult consult-company consult-dir consult-projectile consult-recoll consult-yasnippet embark embark-consult marginalia orderless vertico writeroom-mode org-ref yafolding org-roam json-navigator org-download org-mime neotree persp-projectile perspective elpy gnuplot go-translate command-log-mode org-caldav centaur-tabs treemacs treemacs-all-the-icons treemacs-projectile german-holidays rainbow-delimiters org-chef fill-column-indicator projectile-ripgrep dictcc htmlize org-edna all-the-icons all-the-icons-dired major-mode-icons markdown-changelog markdown-mode+ flymake dashboard lorem-ipsum pass treemacs-icons-dired treemacs-magit outline-magic centered-window json-mode csproj-mode fsharp-mode elpher crontab-mode emacsshot ssh org-pdfview imenu-anywhere nikola dklrt flycheck-ledger epresent 0xc status calfw-cal win-switch corral auto-org-md recentf-ext org-gcal magit-gitflow elfeed-org calfw-gcal ag))
(setq pass-username-fallback-on-filename t)
(setq password-cache-expiry nil)
(setq password-store-password-length 16)
(setq pdf-view-midnight-colors '("#ffffff" . "#100f10"))
(setq persp-mode t)
(setq plantuml-jar-path "/usr/share/plantuml/plantuml.jar")
(setq pos-tip-background-color "#E6DB74")
(setq pos-tip-foreground-color "#242728")
(setq powerline-color1 "#1E1E1E")
(setq powerline-color2 "#111111")
(setq ranger-override-dired 'ranger)
(setq ranger-override-dired-mode t)

(setq recentf-exclude '("agenda" "history" "tmp" "wiki"))
(setq recentf-max-menu-items 40)
(setq recentf-max-saved-items 40)
(setq recentf-menu-filter 'recentf-arrange-by-mode)
(setq recentf-mode t)

(setq require-final-newline t)
(setq revert-without-query nil)
(setq rmail-movemail-program "/usr/bin/movemail")
(setq rmh-elfeed-org-files '("~/org/feeds.org"))
(setq rmh-elfeed-org-tree-id "feeds")
(setq safe-local-variable-values
 '((org-roam-mode . t)
	 (org-roam-directory . "~/org/odo/")
	 (initial-major-mode . dokuwiki-mode)))
(setq same-window-buffer-names '("shell"))
(setq save-abbrevs 'silently)
(setq scroll-bar-mode nil)
(setq send-mail-function 'smtpmail-send-it)
(setq size-indication-mode t)
(setq small-temporary-file-directory "/tmp/")

(setq split-height-threshold 20)
(setq split-width-threshold 70)
(setq split-window-preferred-function 'split-window-really-sensibly)

(setq tab-always-indent 'complete)
(setq tab-width 4)
(setq tabbar-background-color "#357535753575")
(setq tool-bar-mode nil)
(setq tooltip-mode t)
(setq tree-widget-themes-directory "tree-widget")

(setq user-mail-address "dr.ops@mailbox.org")
(setq vc-annotate-background "#3C4C55")
(setq vc-annotate-background-mode nil)
(setq vc-annotate-color-map
 `((20 \, "#DF8C8C")
	 (40 \, "#e3af97978d26")
	 (60 \, "#e780a2a28dc0")
	 (80 \, "#eb50adac8e5a")
	 (100 \, "#ef21b8b88ef4")
	 (120 \, "#F2C38F")
	 (140 \, "#ee20c861905c")
	 (160 \, "#e94eccff912a")
	 (180 \, "#e47dd19d91f7")
	 (200 \, "#dfabd63b92c5")
	 (220 \, "#DADA93")
	 (240 \, "#d0d0d8719393")
	 (260 \, "#c6c6d6089393")
	 (280 \, "#bcbcd39f9393")
	 (300 \, "#b2b2d1369393")
	 (320 \, "#A8CE93")
	 (340 \, "#a13ac894a409")
	 (360 \, "#99ccc25bb480")
	 (380 \, "#925ebc21c4f7")
	 (400 \, "#8af0b5e8d56e")
	 (420 \, "#83AFE5")
	 (440 \, "#8821aa0fe517")
	 (460 \, "#8cbfa470e449")
	 (480 \, "#915d9ed1e37c")
	 (500 \, "#95fb9932e2ae")
	 (520 \, "#9A93E1")))
(setq vc-annotate-very-old-color "#7bae760fb4b4")
(setq vc-follow-symlinks nil)
(setq vdirel-repository "~/.vdir/contacts/contacts")

(setq vertico-mode t)
(setq vertico-multiform-mode t)
(setq vertico-posframe-font "FreeMono")
(setq vertico-posframe-mode t)
(setq vertico-sort-function 'vertico-sort-history-length-alpha)

;;  (setq weechat-color-list   (unspecified "#242728" "#323342" "#F70057" "#ff0066" "#86C30D" "#63de5d" "#BEB244" "#E6DB74" "#40CAE4" "#06d8ff" "#FF61FF" "#ff8eff" "#00b2ac" "#53f2dc" "#f8fbfc" "#ffffff"))
  (setq winner-mode t)
  (setq wl-message-ignored-field-list
   '(".*Received:" ".*Path:" ".*Id:" "^References:" "^Replied:" "^Errors-To:" "^Lines:" "^Sender:" ".*Host:" "^Xref:" "^Content-Type:" "^Precedence:" "^Status:" "^X-*:"))

(setq writeroom-fullscreen-effect 'maximized)
(setq writeroom-global-effects
 '(writeroom-set-alpha writeroom-set-menu-bar-lines writeroom-set-tool-bar-lines writeroom-set-vertical-scroll-bars writeroom-set-bottom-divider-width))
(setq writeroom-major-modes '(elfeed-show-mode elfeed-search-mode))
(setq writeroom-width 100)

(require 'perspective)
(global-set-key (kbd "C-x C-b") 'persp-list-buffers)
(customize-set-variable 'persp-mode-prefix-key (kbd "C-c <f2>"))
(persp-mode)

(require 'projectile)


(define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
(projectile-mode +1)

(setq projectile-project-search-path '("~/prj/"))

(projectile-register-project-type 'nikola '("conf.py")
                  :compile "nikola build"
                  :test "nikola auto"
                  :run "nikola deploy"
                  :test-suffix ".spec")

(require 'vertico)
	  (vertico-mode)
          (vertico-buffer-mode)

	  ;; Enable vertico-multiform
	  (vertico-multiform-mode)

   ;; Configure the display per command.
  ;; Use a buffer with indices for imenu
  ;; and a flat (Ido-like) menu for M-x.
	  (setq vertico-multiform-commands
		    '((consult-imenu buffer indexed)
			  (execute-extended-command grid)
			  (org-refile buffer)
			  ))

	  ;; Configure the display per completion category.
	  ;; Use the grid display for files and a buffer
	  ;; for the consult-grep commands.
	  (setq vertico-multiform-categories
		    '((file grid)
			  (consult-grep buffer)))

  ;; Add prompt indicator to `completing-read-multiple'.
  ;; Alternatively try `consult-completing-read-multiple'.
  (defun crm-indicator (args)
    (cons (concat "[CRM] " (car args)) (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
	    '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  ;; (setq read-extended-command-predicate
  ;;       #'command-completion-default-include-p)

  ;; Enable recursive minibuffers
(setq enable-recursive-minibuffers t)
(setq vertico-buffer-mode t)
(setq vertico-posframe-font "FreeMono")
(setq vertico-posframe-mode nil)

(require 'yasnippet)
(setq yas-snippet-dirs
      '("~/./config/emacs/snippets"                 ;; personal snippets
        ))

(global-set-key (kbd "C-0") 'kill-this-buffer)
(global-set-key (kbd "C-.") 'split-window-horizontally)

(global-set-key (kbd "C-M-f") 'elfeed)
(global-set-key (kbd "C-M-m") 'mu4e)

(require 'selected)
(selected-global-mode)

(defun ekz (&optional b e) 
  (interactive "r")
  (append-to-file b e "~/cloud/Notes/Einkaufszettel.txt"))

(require 'kagi)

  ;; or use a function, e.g. with the password-store package:
   (setq kagi-api-token (lambda () (password-store-get "Kagi/API")))

  ;; Universal Summarizer settings
  (setq kagi-summarizer-engine "cecil")
  (setq kagi-summarizer-default-language "DE")
  (setq kagi-summarizer-cache t)

;; How to use:
;; 1. add `(load "/path/to/next-spec-day")` to your dot emacs file.
;; 2. set `NEXT-SPEC-DEADLINE` and/or `NEXT-SPEC-SCHEDULED` property of a TODO task,like this:
;;         * TODO test
;;           SCHEDULED: <2013-06-16 Sun> DEADLINE: <2012-12-31 Mon -3d>
;;           :PROPERTIES:
;;           :NEXT-SPEC-DEADLINE: (= (calendar-extract-day date) (calendar-last-day-of-month (calendar-extract-month date) (calendar-extract-year date)))
;;           :NEXT-SPEC-SCHEDULED: (org-float 6 0 3)
;;           :END:
;;     The value of NEXT-SPEC-DEADLINE will return `non-nil` if `date` is last day of month,and the value of NEXT-SPEC-SCHEDULED will return `non-nil` if `date` is the fathers' day(the third Sunday of June).
;; 3. Then,when you change the TODO state of that tasks,the timestamp will be changed automatically(include lead time of warnings settings).
;; Notes:
;; 1. Execute `(setq next-spec-day-runningp nil)' after your sexp signal some erros,
;; 2. You can also use some useful sexp from next-spec-day-alist,like:
;; * TODO test
;;   SCHEDULED: <2013-03-29 Fri>
;;   :PROPERTIES:
;;   :NEXT-SPEC-SCHEDULED: last-workday-of-month
;;   :END:
;; 3. If you encounter some errors like 'org-insert-time-stamp: Wrong type argument: listp, "<2013-03-29 星期五>"' when change the TODO state,please try a new version of org mode.To use the new version:
;; (1). download the new version of org mode from orgmode.org,then uncompress it.
;; (2). add (add-to-list 'load-path "/path/to/org-*.*.*/lisp") to your .emacs file,make sure it's before any (require 'org).If you are not sure,just insert it to the first line of your dot emacs file.
;; (3). restart your emacs,everything should be fine.
(eval-when-compile (require 'cl))
(defvar next-spec-day-runningp)
(setq next-spec-day-runningp nil)
(defvar next-spec-day-alist
  '((last-workday-of-month
     .
     ((or
       (and (= (calendar-last-day-of-month m y) d) (/= (calendar-day-of-week date) 0) (/= (calendar-day-of-week date) 6))
       (and (< (- (calendar-last-day-of-month m y) d) 3) (string= (calendar-day-name date) "Friday")))))
    (last-day-of-month
     .
     ((= (calendar-extract-day date) (calendar-last-day-of-month (calendar-extract-month date) (calendar-extract-year date)))))
    (fathers-day
     .
     ((org-float 6 0 3))))
  "contain some useful sexp")
(defun next-spec-day ()
  (unless next-spec-day-runningp
    (setq next-spec-day-runningp t)
    (catch 'exit
      (dolist (type '("NEXT-SPEC-DEADLINE" "NEXT-SPEC-SCHEDULED"))
	(when (stringp (org-entry-get nil type))
	  (let* ((time (org-entry-get nil (substring type (length "NEXT-SPEC-"))))
		 (pt (if time (org-parse-time-string time) (decode-time (current-time))))
		 (func (ignore-errors (read-from-whole-string (org-entry-get nil type)))))
	    (unless func (message "Sexp is wrong") (throw 'exit nil))
	    (when (symbolp func)
	      (setq func (cadr (assoc func next-spec-day-alist))))
	    (cl-incf (nth 3 pt))
	    (setf pt (decode-time (apply 'encode-time pt)))
	    (cl-do ((i 0 (1+ i)))
		((or
		  (> i 1000)
		  (let* ((d (nth 3 pt))
			 (m (nth 4 pt))
			 (y (nth 5 pt))
			 (date (list m d y))
			 entry)
		    (eval func)))
		 (if (> i 1000)
		     (message "No satisfied in 1000 days")
		   (funcall
		    (if (string= "NEXT-SPEC-DEADLINE" type)
			'org-deadline
		      'org-schedule)
		    nil
		    (format-time-string
		     (if (and
			  time
			  (string-match
			   "[[:digit:]]\\{2\\}:[[:digit:]]\\{2\\}"
			   time))
			 (cdr org-time-stamp-formats)
		       (car org-time-stamp-formats))
		     (apply 'encode-time pt)))))
	      (cl-incf (nth 3 pt))
	      (setf pt (decode-time (apply 'encode-time pt)))))))
      (if (or
	   (org-entry-get nil "NEXT-SPEC-SCHEDULED")
	   (org-entry-get nil "NEXT-SPEC-DEADLINE"))
	  (org-entry-put nil "TODO" (car org-todo-heads))))
    (setq next-spec-day-runningp nil)))
(add-hook 'org-after-todo-state-change-hook 'next-spec-day)
(unless (fboundp 'read-from-whole-string) (require 'thingatpt))
(unless (fboundp 'calendar-last-day-of-month) (require 'thingatpt))

(server-start)

  (find-file "~/org/ops.org") 
  (setq my-timer (run-with-idle-timer 300 t 'org-store-agenda-views))
  (treemacs-find-file)

(if (daemonp)
    (add-hook 'after-make-frame-functions
	(lambda (frame)
	    (select-frame frame)
	    (load-theme 'DrOps t)))
    (load-theme 'DrOps t))
