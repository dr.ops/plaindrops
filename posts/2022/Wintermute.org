#+BEGIN_COMMENT
.. title: Operation Wintermute
.. description: In diesem Winter lernen wir Energie sparen
.. tags: energy
.. keywords: gas, strom, winter, energie, energiesparen
.. slug: wintermute
.. date: 2022-09-19 09:00:00 UTC+02:00
.. category: transitions
#+END_COMMENT

in den Nachrichten ist es aktuell überall Thema: Die steigenden Kosten für Gas und Strom und andere Energieträger. Dabei hatte ich bisher sogar noch Glück. Ich hatte relativ langfristige Verträge,  die erst in den kommenden Wochen auslaufen. Im Moment treffen bei mir die ersten Briefe ein, die mich darauf vorbereiten sollen. Also bereite ich mich darauf vor,  indem ich schaue wo  ich in diesem Winter Energie sparen kann.
{{{TEASER_END}}}

Da die Verbrauchswerte messbar sind, bietet es sich an, das ganze zu gameifizieren. Das erhöht die Motivation, und wenn es da noch einen coolen Projektnamen wie "Operation Wintermute" kriegt, hoffe ich, dass ich dran bleibe. Die Chancen stehen gut, da ich ja auch in den vergangenen Jahren schon regelmäßig meine Zählerstände abgelesen habe, und immer wieder beim Verbrauch nachgesteuert.

* Der IST-Zustand
Ich lebe in einem kleinen Einfamilienhaus mit 117qm Wohnfläche. Bis Herbst letzten Jahres wohnten wir hier noch zu zweit doch seit meine Tochter zum studieren weggezogen ist, habe ich das Haus für mich alleine. Mein Arbeitsplatz und meine Freundin sind beide in der selben, etwa fünfzig Kilometer entfernten Stadt, welche leider nicht vernünftig mit öffentlichen Verkehrsmitteln an meinen Wohnort angebunden ist.

** Strom
Beim Strom habe ich aktuell einem Grundpreis von 11 Euro im Monat und einen Verbrauchspreis pro Kilowattstunde von 29,91 Cent. Das bedeutet, ich habe aktuell noch keine Putin Erhöhung erhalten

Der Verbrauch hat sich in den letzten Jahren so enwickelt:


| Jahr |     kWh |
|------+---------|
| 2018 |    2381 |
| 2019 |    2390 |
| 2020 |    2731 |
| 2021 |    2788 |
| 2022 | ca 2400 |

Die Zahl für 2022 kann ich bisher nur schätzen,  da die Abrechnung etwa Mitte Dezember erfolgen wird. Da mein Verbrauch über das Jahr allerdings weitgehend linear erfolgt habe ich ihn anhand der bisherigen Zählerablesung extrapoliert. 
Die knapp 400 Kilowattstunden mehr die in den Jahren 2020/21 in der Liste stehen schiebe ich auf das Homeoffice. Ich habe seither hier natürlich öfter die Beleuchtung im Arbeitszimmer laufen,  und auch der Laptop trägt seinen Teil dazu bei. Damit läge ich für das Jahr 2022 bei 850 € Gesamtkosten, und einem Abschlag von etwa 71 €.


** Gas
Beim Gas habe ich aktuell einem Grundpreis von 4,33 € im Monat und einen Verbrauchspreis pro Kilowattstunde von 6,3 Cent. Ab dem 1. November kommen die Gasbeschaffungs- und die Gasspeicher-Umlage dazu,  sodass mein Verbrauchspreis auf 10,2 Cent/kWh steigt. Die gestiegenen Beschaffungskosten hat mein Versorger bisher noch nicht weitergegeben. Ich denke er wird damit bis kurz vor Ende meines derzeitigen Vertrages warten, um einen möglichst genauen Stand weitergeben zu können.

Der Verbrauch hat sich in den letzten Jahren so enwickelt:


| Jahr |                  kWh |
|------+----------------------|
| 2018 |                 9840 |
| 2019 |                10689 |
| 2020 |                12723 |
| 2021 |                10447 |
| 2022 | noch keine Schätzung |

Die Zahl für 2022 kann ich bisher noch nicht schätzen, da mein Verbrauch über das Jahr leider nicht linear erfolgt. Die Aufstellung einer entsprechend gewichteten Verbrauchskurve ist einer der nächsten Schritte die ich vorhabe. 
Bei einem Verbrauch vergleichbar zum Vorjahr, läge ich für das Jahr 2022 bei 775 € Gesamtkosten, und einem Abschlag von etwa 65 €.

Durch die Verbrauchspreiserhöhung für die letzten beiden Monate des Jahres, werde ich vermutlich eine Nachzahlung haben. Da ich aber im letzten Jahr eine kleine Rückzahlung hatte und schon abzusehen war,  dass sich die Preise erhöhen werden, habe ich diese Rückzahlung in meinem Gas-Budget belassen, Ich gehe davon aus, dass diese Rückzahlung die Nachzahlung etwa ausgleicht.

** Benzin

Den Benzinverbrauch für mein Auto beobachte ich derzeit von allen Verbräuchen am ungenauesten. Was ich weiß, ist, dass mein Auto im Mischbetrieb mit einer Strecke die etwa zur Hälfte aus Autobahn besteht circa 4,2 l/100km verbraucht, wenn ich es extrem sparsam fahre ich etwa auf 3,8 l/100km komme. Im Moment mache ich es so, dass ich ein Benzin Budget von 180€ im Monat habe, von dem am Ende des Monats immer noch etwas übrig ist. Das übrige Geld buche ich derzeit auf das Budget für Wartung und Reparaturen um. Ich tanke also für weniger als 180€ im Monat. Ich würde schätzen dass es aktuell etwa 130€-140€ sind. In der nächsten Zeit werde ich vermutlich einmal pro Woche zur Arbeit und zweimal pro Woche zu meiner Freundin fahren. Es besteht allerdings die Möglichkeit die Fahrt zur Arbeit mit einer Fahrt zu meiner Freundin zu kombinieren, da beide im gleichen Ort sind.

* Der SOLL-Zustand

Schön wäre es, wenn ich durch die Energieeinsparungen so viel Geld einsparen könnte, dass sich die Preiserhöhung damit auffangen ließe,  davon gehe ich allerdings noch nicht wirklich aus.  Beim Strom würde ich es gerne schaffen auf 2000 kWh zu kommen beim Gas etwa 10.000 kWh und beim Auto wäre ich gerne mittelfristig bei 150€ im Monat für Benzin.
