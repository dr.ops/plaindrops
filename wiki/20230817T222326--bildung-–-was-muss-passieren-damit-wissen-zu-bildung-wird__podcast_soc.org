#+BEGIN_COMMENT
.. title: Bildung – Was muss passieren damit Wissen zu Bildung wird?
.. slug: 20230817T222326--bildung-–-was-muss-passieren-damit-wissen-zu-bildung-wird__podcast_soc
.. tags: podcast:soc
.. keywords: podcast:soc
.. status: private
.. category: wiki
#+END_COMMENT

* Soziopod #002: Bildung – Was muss passieren damit Wissen zu Bildung wird?

- Download:
- [[http://cdn.podseed.org/soziopod/SOZPOD002.mp3][mp375 MB]]

Herr Breitenbach und Doktor Köbel diskutieren über das Thema “Bildung – Was muss passieren damit Wissen zu Bildung wird?”

Was ist Bildung? Was ist der Unterschied zwischen Faktenwissen und Bildungsprozessen? Welche Rolle spielt die Wikipedia? Und was können wir generell tun, damit das Internet zu einem sinnvollen Bildungswerkzeug wird?

Links zur Sendung:

Tolle Mindmap zur Sendung von [[http://kaizenssummera.blogspot.de/][Sven Sönnichsen]]:  
[[http://soziopod.de/wp-content/uploads/2011/10/Mind-Map_BILDUNG.jpg][http://soziopod.de/wp-content/uploads/2011/10/Mind-Map_BILDUNG-300x202.jpg]]

[[http://www.hwr-berlin.de/fileadmin/downloads_internet/publikationen/Birie_Gebildet_sein.pdf][Peter Birie: Wie wäre es gebildet zu sein]] Peter Biries Bildungsbegriff.

[[http://de.wikipedia.org/wiki/Bildung][Wikipedia zu „Bildung“]]

[[http://www.welt.de/kultur/article12322845/Amerika-berauscht-sich-an-brachialen-Erziehungstipps.html][„Amerika berauscht sich an brachialen Erziehungstipps“]] Amy Chua und ihr Schlachtgesang als Tigermama. Der Anfang vom Ende der Bildung.

[[http://www.sueddeutsche.de/digital/siegfried-kauder-zum-urheberrecht-im-internet-meine-blasmusik-interessiert-das-nicht-1.1169431][Volker Kauder im Interview: Das Internet als dunkler Ort.]]

Vortrag von Salman Khan: [[http://www.ted.com/talks/salman_khan_let_s_use_video_to_reinvent_education.html][Let’s use video to reinvent education]] (Das ist der Hedgefondsmanager von dem ich dir erzählt habe. Sehr spannend. Auf alle Fälle ganz gucken, nicht nur spulen)

[[http://smarthistory.khanacademy.org/][Neuer Geschichtsunterricht bei Khan Academy]] (Das ist ein neues Projekt von diesem oben genannten Khan)
