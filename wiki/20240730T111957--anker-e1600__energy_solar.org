#+BEGIN_COMMENT
.. title: Anker E1600
.. slug: 20240730T111957--anker-e1600__energy_solar
.. tags: energy:solar
.. keywords: energy:solar
.. status: private
.. category: wiki
#+END_COMMENT

Hallo „Solarbank“-Mitstreiter,
Aufgrund der leider nicht vorhandenen Dokumentation von ANKER zum Ladeverhalten der „Solarbank“ waren wir alle ja
reichlich gefrustet, was man dann in der Praxis am „lebenden Objekt“ sehen konnte - und zunächst erst mal in keinerlei
„LOGIK“ packen konnte. Aufgrund unsere gemeinsamen Erfahrung und meiner eigenen Beobachtung probiere ich mal das
folgende zu offenbar seitens ANLER programmierte „Lade-Algorythmus“ zusammen zu fassen.
Das BKW besteht aus:
• 2x 375 PV-Modulen (LONGI LR4-60HIH-375)
• 1x MWR Hoymiles HM-600
• 1x ANKER Solix Solarbank E1600
Einstellungen:
• App V1.8.2 (Android) - Firmware (Solarbank) V1.4.1 (<- verfügbar ab 30.10.2023)
• MWR-Einstellung (Solarbank): ANKER „A5143“ (<- da derzeit NUR damit „Familienladung“ = min. 100W möglich)
Ladeverhalten:
• DC (aus PV) < ~ 15-20W: Solarbank „schläft“
• DC (aus PV) > ~ 15-20 W bis ~ 45-50 W: Solarbank lädt (unabhängig von Einstellung „Familienladung“)
• DC (aus PV) > ~ 45-50W bis 100W *: Bypass läuft (= DC-Weiterleitung an MWR)
• DC (aus PV) > 100W: „Split-Betrieb“: 100W „Familienladung“ (= Ausgabe an MWR) erfolgt OBLIGAT im Bypass
PLUS „Überschuß“ (= abzüglich der 100W*) dann als Akku-Ladung
*: 100W bei MWR=“A5143“, bei allen anderen sonst 150W
• DC (aus PV) > 100W: „Split-Betrieb“: ist temperaturabhängig bzgl. der möglichen Ladeleistung der „Solarbank“
 < 0°C bis ~ 5°C: KEINE Ladung möglich! (AkkuChemie LiFePO4)
 bis V1.3.8 (10/2023):
 < ~ 13° C: ~ 160W
 < ~ 23° C: ~ 310W
 > ~ 24° C: ~ 600 bis 700W (<- abhängig vom DC-Input ./. „Familienladung“)
 ab V1.4.1 (11/2023): > ~ 13° C: bis 600W (<- abhängig vom DC-Input ./. „Familienladung“)
„Familienladung“:
• gemeint: Leistungsausgabe der Solarbank an den MWR
• paralleles Laden-/Entladen ist bauseitig (<- derzeit!??) NICHT möglich
• Entladung aus „Solarbank“ (= „Familienladung“) erfolgt NUR, wenn KEIN DC-Input aus PV-Modulen (= 0W nachts)
• Tagsüber (= DC-Input aus PV-Modulen) wird die FIXE-Einstellung einer „Familienladung“ systemseitig „überspielt“
-> s.o.: „Splitbetrieb“ aus Bypass bei DC-Eingang > 100/150W* (s.o. Einstellung MWR ANKER „A5143“) d.h.:
• Lade-„Überschußabhängig“ erfolgt (= DC ist größer als Bypass 100W/150W) UND
• Zudem (ab V1.4.1) noch bedingt temperaturabhängig ist (<- wer ein E-Auto fährt, kennt das schon)
-> ab V1.4.1 (11/2023): > ~ 13° C: bis 600W Ladung, wenn DC-Eingang der Module ~ 700W
-> dann ~ 600W Ladeleistung der „Solarbank“ + den OBLIGATEN Bypass ins Haus von 100/150W
-> „Priorisierung“ der Ladeleistung 10%-100% einstellbar (wenn „A5143“ in APP hinterlegt)
• Wenn ANKER „5143“ tatsächlich vorhanden ist:
• Bypass entfällt bis zum eingestellten SOC (z.B. 80% = NUR Ladung, Bypass erst ab 80% SOC)
• Nachts (= KEIN DC-Input aus PV-Modulen) startet dann die eingestellte „Familienladung“
 Zum eingestellten Zeitpunkt (z.B. „20Uhr“) dann mit
 100W (Einstellung ANKER „A5143“) <- das funktioniert derzeit zuverlässig auch, wenn „tatsächlich“
ein anderer MWR angeschlossen ist)
 150W bei (derzeit) allen anderen MWR
 200W = DEFAULT, wenn die „Familienladung“ ausgeschaltet wird.
Soviel mal als Versuch eine „überschaubare“ Systematik in das beschriebene und selbst beobachtete hereinzubringen.
Man kann also - bei allen „Unzulänglichkeiten“ - nicht sagen, das System macht im Lade-/Entlade-Verhalten „was es will“.
 bitte ergänzt oder korrigiert, wenn es andere Beobachtungen in der Community dazu gibt.
 Ich sollte an dieser Stelle einfach mal aus dazu bisher vielfach beschriebenen eine möglichst übersichtliche
& plausible Zusammenstellung des Lade-/Entladeverhaltens erstellen
 ... in der Hoffnung, etwas „Plausibilität“ in das Ganze zu bringen ....
Mangels hinreichender Doku-/Information seitens ANKER wird allerdings (und das auch bei mir !!!) der „Anfangs-
„Enthusiasmus“ herb enttäuscht, mit dem man etwas „blauäugig“ davon ausging, dass quasi die gesamte tagsüber via PV-
Modul erzeugte DC-Leistung „in toto“ einfach mal gespeichert und nachts dann wieder nutzbar ins Haus aus eingespeist wird.
So „simpel“ ist es mit der ANKER Solarbank nun nicht ...
Andrerseites: i.R. des o.g. genannten funktioniert’s bei mir im aktuellen „Herbstbetrieb“ aber doch ganz gut – zumal man nach
den „Ersteinstellungen“ als „bequemer Nutzer“ dann nichts mehr „anfassen“ muß.
-> Was nicht heißt, dass ich nicht auch darauf hoffe, dass ANKER hier für „Fremd-MWR“ weiter nachjustiert
-> insbesondere was die „Priorisierung“ der Ladeleistung (= kein Bypass) auch für „NICHT-ANKER“-MWR angeht
