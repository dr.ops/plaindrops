#+BEGIN_COMMENT
.. title: Albert Schnez
.. slug: 20240723T002322-rightwing--albert-schnez__fasc_person
.. tags: fasc:person
.. keywords: fasc:person
.. status: private
.. category: wiki
#+END_COMMENT

Die zentrale Figur der [[https://plaindrops.de/wiki/20240723T000841-rightwing--schnez-truppe__orga][Schnez-Truppe]] war Albert Schnez, der im Zweiten Weltkrieg als Oberst der Wehrmacht diente, bevor er in die Ränge der 1955 gegründeten westdeutschen Streitkräfte, der Bundeswehr, aufstieg. Ende der 1950er Jahre gehörte Schnez zum Gefolge des damaligen Verteidigungsministers Franz Josef Strauß (CDU) und diente später als Chef der Bundeswehr unter Bundeskanzler Willy Brandt und Verteidigungsminister Helmut Schmidt (beide SPD).
