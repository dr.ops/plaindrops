#+BEGIN_COMMENT
.. title: Reichtum
.. slug: 20231117T171022--reichtum__fin_soc
.. tags: fin:soc
.. keywords: fin:soc
.. status: private
.. category: wiki
#+END_COMMENT

Wir haben keinen Schimmer, wie reich Superreiche wirklich sind. Das hat System

Die Ungleichheit in Deutschland ist enorm. Doch Ideen für mehr Fairness scheitern schon daran, dass es kaum Daten über Hochvermögende gibt. Damit muss Schluss sein.

Anfang November war es wieder so weit: Die ranghöchsten Wirtschaftsberater:innen der Bundesregierung stellten ihren Jahresbericht vor. Dieser hatte viele schlechte Nachrichten im Gepäck: schwächelndes Wachstum, ein reformbedürftiges Rentensystem und wachsende Armut.

Kurz: Es läuft aktuell nicht rund in Deutschland.

Doch die sogenannten »Wirtschaftsweisen«[fn:1] klagen in ihrem Bericht nicht nur, sondern haben auch eine Reihe von Lösungsvorschlägen erarbeitet. Ein zentrales Element: Sowohl Staat als auch Unternehmen müssen Geld in die Hand nehmen und in die Zukunft investieren.

Woher das Geld kommen soll, dazu sagten die Berater:innen nichts. Jedenfalls bis die Runde für Fragen der anwesenden Journalist:innen geöffnet wurde [[https://www.jungundnaiv.de/][und Tilo Jung das Mikrofon ergriff]]:

#+begin_quote
Thema Armut und Armutsgefährdung: Ich habe in ihrem Bericht nach dem Abschnitt Vermögensungleichheit gesucht. Das ist mal wieder kein Thema bei Ihnen, dabei werden in Deutschland Milliardäre immer reicher. Gleichzeitig ist bekannt, dass die Vermögensungleichheit für die Wirtschaft schädlich ist. […] Sind Sie auf diesem Auge blind?
Tilo Jung vom Youtube-Kanal »Jung & Naiv«
#+end_quote


[[https://www.youtube.com/live/NmOGUtrTFlM?feature=shared&t=4412][Die Antwort der Vorsitzenden der Wirtschaftsweisen lässt tief blicken]], wo wir in Deutschland in Sachen Verteilungsgerechtigkeit stehen – und müsste (eigentlich) für einen handfesten Skandal taugen:

#+begin_quote
Wir wollten unbedingt etwas über die Vermögensverteilung schreiben. Wir haben aber die Daten schlichtweg nicht. Man kann keine Entwicklung darstellen, wenn sie nicht darstellbar ist. Wir sprechen das in jedem Gespräch im Finanzministerium an, wir sprechen es jedes Mal im Bundeskanzleramt an. […] An der Stelle scheitern wir einfach an den Daten.Monika Schnitzer, Vorsitzende des Sachverständigenrates zur Begutachtung der gesamtwirtschaftlichen Entwicklung
#+end_quote

Ja, richtig gelesen. Renommierte Wirtschaftsexpert:innen der drittgrößten Volkswirtschaft der Welt haben keine belastbaren Daten über Vermögen in Milliardenhöhe. Weder darüber, wie sie sich in den letzten Jahren entwickelt haben, noch darüber, wie sie genau verteilt sind. Und schon gar nicht darüber, wie absurd groß sie tatsächlich sind. Nichts.

Und das in einem Land, in dem es Statistiken zu allem Möglichen gibt – von Verkaufszahlen und Preisen der neuesten SUV-Modelle über Übernachtungszahlen im Harz bis hin zur monatlich minutiös berechneten Inflationsrate. Und in einem Land, in dem in Sachen Einkommensteuer jedes noch so kleine Gehalt vom Finanzamt auf Heller und Pfennig überprüft wird.

Das ist kein Zufall.

Ist es politisch etwa nicht gewollt, Licht ins Dunkel der Vermögensverhältnisse der Reichen und Mächtigen im Land zu bringen?


** Was du nicht weißt, macht dich nicht heiß

Wer genau wissen will, wie beklagenswert dünn die Datenlage in Deutschland für Forschende tatsächlich ist, braucht nur einen Blick in das [[https://www.sachverstaendigenrat-wirtschaft.de/fileadmin/dateiablage/gutachten/jg202324/JG202324_Gesamtausgabe.pdf#page=420&zoom=200][aktuelle Jahresgutachten der Wirtschaftsweisen zu werfen.]]
Mit dem Kapitel »Zeitgemäße Dateninfrastruktur für fundierte Entscheidungen« haben die Autor:innen dem Thema in diesem Jahr einen komplett eigenen Abschnitt gewidmet.

Die Mängelliste ist lang – und zwar nicht nur im Bereich Ungleichheit, sondern etwa auch zu den Themen Bildung, Energie und vielem mehr. Die für Forschung grundlegenden Daten seien häufig lückenhaft und veraltet, die Verfügbarkeit eingeschränkt und der Zugang nicht benutzerfreundlich. Zwar habe es in den vergangenen Jahren auch kleinere Fortschritte gegeben, alles in allem sei die deutsche Forschungsdateninfrastruktur »im internationalen Vergleich jedoch nach wie vor rückständig«, schreiben die Wirtschaftsweisen.

Eine niederschmetternde Bestandsaufnahme – die einer kleinen Gruppe von Menschen durchaus zupasskommt: Denn solange es keine verlässliche Datengrundlage gibt, mit der sich die Milliardenvermögen der Superreichen genau(er) beziffern lassen, sind alle Diskussionen über verteilungspolitische Maßnahmen rund um Vermögen- und Erbschaftsteuer von vornherein zum Scheitern verurteilt.

Aber wie kann das sein?

** Vermögensdaten in Deutschland: Warum die Superreichen fehlen

Der Wirtschaftsjournalist und Direktor der Initiative Forum New Economy, [[https://newforum.org/vom-verdraengten-thema-ungleichheit-zum-vermoegens-simulator/][Thomas Fricke, weiß, warum so viele Wissenslücken bei großen Vermögen herrschen]]. Er schreibt, dass der Glaube an das Versprechen vom »Wohlstand für alle« lange Zeit derart tief saß, dass kaum jemand genauer hinschaute, wie »Reich« und »Arm« wirklich leben.

Das ging so weit, dass Statistiken zur Ungleichheit in der Bundesrepublik Deutschland lange Jahre vor allem von Forschenden aus dem Ausland kamen. Der Wunsch nach eigenen amtlichen Statistiken zum Thema sei, so Fricke, mehrfach von der Politik abgelehnt worden.

[[https://perspective-daily.de/article/792-du-zahlst-zu-viele-steuern-aber-aus-anderen-gruenden-als-du-denkst/][Als  dann 1997 die Vermögensteuer von der Regierung Kohl ausgesetzt wurde]], knallten die Champagnerkorken in den Villen der Republik gleich doppelt: Nicht nur, dass große Vermögen seither in Deutschland gar nicht mehr besteuert werden – es werden auch keine Daten mehr über den Umfang von Portfolios, Jacht- und Privatjetfuhrparks oder Kunstsammlungen gesammelt.

Erst seit 2001 gibt es einen offiziellen Armuts- und Reichtumsbericht der Bundesregierung – der allerdings stark auf Armut fokussiert ist. So verwundert es nicht, dass auch dieser Bericht schmallippig wird, wenn es zum Thema Vermögen kommt. Hier heißt es:

#+begin_quote
Offizielle Registerdaten zur Vermögenssituation liegen für Deutschland nicht vor. Analysen zur Vermögensungleichheit sind auf Stichprobenbefragungen angewiesen. Diese untererfassen tendenziell die Hochvermögenden, auf die aber ein beträchtlicher Teil des Gesamtvermögens entfällt.Armuts- und Reichtumsbericht der Bundesregierung, 2023
#+end_quote

Die einzigen verfügbaren Daten stammen aus vereinzelten Stichproben des Statistischen Bundesamtes, der Bundesbank und vom Deutschen Institut für Wirtschaftsforschung (DIW). Letzteres liefert mit dem Sozio-oekonomischen Panel (SOEP),Das Sozio-oekonomische Panel (SOEP) ist eine repräsentative Wiederholungsbefragung von Privathaushalten in Deutschland. Die Befragung wird im jährlichen Rhythmus seit 1984 immer bei denselben Personen und Familien durchgeführt (wobei neue Samples über die Zeit hinzukamen). Es nehmen etwa 14.000 Haushalte und 30.000 Personen teil. für das jährlich 14.000 Haushalte in Deutschland befragt werden, die wohl tauglichsten Daten.

Doch diese Erhebungen weisen seit je Defizite auf, die unter Expert:innen weithin bekannt sind. So ist die Teilnahme freiwillig [[https://link.springer.com/article/10.1007/s10797-019-09578-1][und Superreiche somit nachweislich unterrepräsentiert]]. Logisch, denn über Geld spricht man bekanntlich nicht – was umso mehr gilt, je reicher man ist. Sonst entwickeln Menschen, die weniger in Saus und Braus leben, nur Neid und kommen auf politische Ideen …

Wenn vereinzelt doch Hochvermögende an solchen Befragungen teilnähmen, seien die Angaben alles andere als verlässlich, warnen die Wirtschaftsweisen: »Selbsteinschätzungen können insbesondere bei wenig liquidem Vermögen deutlich vom tatsächlichen Vermögenswert abweichen«, heißt es dazu im Jahresbericht.

** Vermögensdaten: Not macht erfinderisch

Um trotz alledem überhaupt zum Thema forschen zu können, mussten Interessierte in der Vergangenheit auf Schätzungen zurückgreifen. Manche davon wurden durch viel individuelles Engagement beständig weiter verfeinert.

Der letzte große Wurf gelang im Jahr 2020. Forscher:innen des Deutschen Instituts für Wirtschaftsforschung (DIW) schlossen damals erstmalig die sogenannte »Millionär:innen-Datenlücke«, indem sie bestimmte Daten stärker gewichteten und hochrechneten. Das Ergebnis: Die Superreichen sind noch wesentlich reicher als zuvor ohnehin schon angenommen.

Im Durchschnitt besitzt jede:r der 1.000.000 Millionär:innen in Deutschland rund 3 Millionen Euro. 94% von ihnen leben in den alten Bundesländern. Der Vergleich des DIW mit einer durchschnittlichen erwachsenen Person aus den unteren 50% der Vermögensverteilung zeigt, wie weit hier die Welten auseinanderliegen: diese besitzt gerade einmal 0,1% davon auf dem Bankkonto – Ersparnisse von 3.682 Euro.

Ungeachtet dieser Erkenntnisse tut sich seither nichts. Auch wenn die neuen Daten gemeinhin als belastbar angesehen werden, fehlt ihnen der Stempel einer offiziellen amtlichen Statistik. Und damit eine unstrittige Basis, auf der weitere Forschung betrieben und verteilungspolitische Maßnahmen vorangetrieben werden könnten. Und so grüßt Jahr für Jahr das Murmeltier und gibt zu Protokoll: Wir haben leider keine belastbaren Daten. Schade aber auch!

** Wer über Armut redet, darf über Reichtum nicht schweigen

Besonders bizarr wird die Diskussion um Reiche immer dann, wenn diese von sich als »gehobener Mittelstand« reden, etwa der CDU-Bundesvorsitzende Friedrich Merz. [[https://www.welt.de/politik/video184068236/Kandidat-um-CDU-Vorsitz-Friedrich-Merz-legt-Millionengehalt-offen.html?wtrid=onsite.onsitesearch][Sein Vermögen könnte bis zu 12 Millionen betragen, das ist aber natürlich nur ein reiner Schätzwert]]. Relativ sicher ist, dass Merz Millionär ist. Er selbst gibt an, eine Million Euro pro Jahr zu verdienen – zur »Oberschicht« will er aber nicht zählen. Ja, klar. Auch solche geistigen Verrenkungen sind nur möglich, weil hierzulande so viel Unwissen über Reichtum herrscht.

Dabei gibt es eine Reihe von Staaten, die Superreiche nicht so leicht davonkommen lassen. In den skandinavischen Ländern gibt es beispielsweise umfassende Registerdaten über Personen und Unternehmen, die unkompliziert verfügbar und auf Anfrage im wissenschaftlichen Interesse miteinander verknüpfbar sind.

Und zwar in allen wichtigen Bereichen, die von gesamtgesellschaftlichem Interesse sind: etwa Daten über den Arbeitsmarkt, das Bildungs- und Gesundheitswesen oder auch zur Einkommens- und Vermögenssituation. In Österreich und Frankreich werden ähnliche Möglichkeiten angeboten.

Zugang haben vor allem Forschende und je nach Land auch Medienvertreter:innen, wie etwa in Norwegen.

/Hier berichten wir über den radikalen Transparenzansatz in Norwegen:/

Auch wenn das Modell »Steuerporno« auf den ersten Blick befremdlich wirkt und wahrscheinlich nicht nur Datenschutzaktivist:innen zu weit gehen dürfte, gibt es noch zahlreiche moderate Hebel, um die klägliche Datenlage in Deutschland zu verbessern – auch wenn diese deutlich weniger sexy sind.

In ihrem Jahresbericht nennen die Wirtschaftsweisen 5 davon:

1. Datenlücken durch neue Datenerhebungen schließen.
2. Zeitverzug bei der Bereitstellung von Daten verkürzen und die Reaktionsschnelligkeit der Statistik erhöhen.
3. Verfügbarkeit bereits erhobener Daten verbessern.
4. Möglichkeiten der Verknüpfung, insbesondere zwischen Datensätzen verschiedener Datenproduzenten, erweitern.
5. Datenzugang deutlich einfacher, moderner und benutzerfreundlicher ausgestalten.

** Ohne Vermögensdaten sind alle Umverteilungsversprechen nicht mehr als politisches Theater

All das klingt nicht unbedingt nach Hexenwerk.

#+begin_remark
*** Womit wirbt die SPD im Umgang mit Superreichen?

SPD-Parteichef Lars Klingbeil stellte Anfang der Woche Pläne vor, um die Einkommen-, Erbschaft- und Schenkungsteuer umfangreich zu reformieren, so »dass Multimillionäre und Milliardäre mehr zum Gemeinwohl beitragen.«
#+end_remark

Die gute Nachricht: Auf der Bundespressekonferenz beteuerten die Wirtschaftsweisen, dass es in einigen der genannten Bereiche durchaus Entwicklungen und Fortschritte gebe. Doch angesichts der gleichzeitig erheblichen Rückständigkeit der Dateninfrastruktur gelte es hier zu klotzen, anstatt nur zu kleckern – idealerweise mit politischer Unterstützung von ganz oben. Schließlich gehe es hier nicht um Peanuts, sondern um mehrstellige Milliardenbeträge.

Dass ein Bundesfinanzministerium unter FDP-Führung daran kein großes Interesse zu haben scheint, dürfte niemanden überraschen.

Überraschender ist, dass die Wirtschaftsweisen auch bei Olaf Scholz, einem Sozialdemokraten (der im Übrigen zuvor das Finanzministerium geführt hat), [[https://taz.de/SPD-Vorstoss-zu-Steuerreform/!5968360/][auf taube Ohren stoßen]]. Wenn es seine SPD mit Blick auf die Bundestagswahl 2025 mit ihrer Umverteilungsrhetorik ernst meint, muss sie dringend Licht ins Dunkel der Vermögensdaten dieser Superreichen bringen.

Schafft sie das nicht, ist alles Weitere reines politisches Theater, bei dem es am Ende wieder heißt: Vermögensdaten in Deutschland? Fehler 404 – Seite nicht gefunden.[fn:2]

* Footnotes

[fn:1] Der Rat der 5 Wirtschaftsweisen (offiziell: Sachverständigenrat zur Begutachtung der gesamtwirtschaftlichen Entwicklung) ist ein im Jahr 1963 eingeführtes Gremium. Als ausschließlich wissenschaftliches Gremium unabhängiger Experten stellen sie der Bundesregierung jährlich ihre Begutachtung und Analyse der wirtschaftlichen Lage Deutschlands vor. Dabei sollen die Wirtschaftsweisen eine rein beratende Funktion für die Politik einnehmen und keine Empfehlungen aussprechen – dieses Verbot wird allerdings sehr flexibel interpretiert.

[fn:2]Der Fehlercode 404 wird in Webbrowsern ausgegeben, wenn eine Website nicht erreichbar oder vorhanden ist.
